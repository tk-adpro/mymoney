## MyMoney

[![pipeline status](https://gitlab.com/tk-adpro/mymoney/badges/master/pipeline.svg)](https://gitlab.com/tk-adpro/mymoney/-/commits/master)

[![coverage report](https://gitlab.com/tk-adpro/mymoney/badges/master/coverage.svg)](https://gitlab.com/tk-adpro/mymoney/-/commits/master)

MyMoney merupakan suatu bot line dimana pengguna dapat mengatur keuangannya di sini.

Link: https://line.me/R/ti/p/%40511byiso

Id Bot Line: 511byiso

Fitur:

1. **Catat Pendapatan** (Finished)
    - Muhammad Luthfi Fahlevi (1906293215)
2. **Catat Pengeluaran** (Finished)
    - Kevin Dharmawan (1906398515)
3. **Notifikasi** (Fitur tidak diterapkan karena tidak hadir sejak awal pertemuan)
    - Dimas Saputra (1506688821)
4. **Laporan** (Finished)
    - Maheswara Ananta Argono (1906398471)
5. **Kalkulator Investasi** (Finished)
    - Linus Abhyasa Wicaksana (1906398761)
    
