package com.bot.mymoney.repository;

import com.bot.mymoney.database.RecordDao;
import java.util.ArrayList;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class RecordRepositoryTest {
  @Mock
  private RecordDao recordDao;

  @InjectMocks
  private RecordRepository recordRepository;

  @Test
  void saveRecordTest() {
    String description = "userId;luthfi;Usaha;10000";
    recordRepository.saveRecord(description);
    when(recordDao.saveRecord("userId", "luthfi",
        "Usaha", "timestamp", "10000"))
        .thenReturn(1);

    recordDao.saveRecord("userId", "luthfi",
        "Usaha", "timestamp", "10000");
    verify(recordDao).saveRecord("userId", "luthfi",
        "Usaha", "timestamp", "10000");
  }

  @Test
  public void testRecordServiceGetAll() {
    when(recordDao.get()).thenReturn(new ArrayList<>());
    Assertions.assertTrue(recordRepository.getAll() instanceof ArrayList);
  }

  @Test
  void getByUserIdTest() {
    when(recordDao.getByUserId("userId")).thenReturn(new ArrayList<>());
    Assertions.assertTrue(recordRepository.getByUserId("userId") instanceof ArrayList);
  }
}
