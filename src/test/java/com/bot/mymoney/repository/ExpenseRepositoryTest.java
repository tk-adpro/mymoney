package com.bot.mymoney.repository;

import com.bot.mymoney.database.ExpenseDao;
import java.util.ArrayList;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ExpenseRepositoryTest {

  @Mock
  private ExpenseDao expenseDao;

  @InjectMocks
  private ExpenseRepository expenseRepository;

  @Test
  void testExpenseGetAllTest() {
    when(expenseDao.get()).thenReturn(new ArrayList<>());
    Assertions.assertTrue(expenseRepository.getAll() instanceof ArrayList);
  }

  @Test
  void testExpenseGetByUserIdTest() {
    when(expenseDao.getByUserId("userid")).thenReturn(new ArrayList<>());
    Assertions.assertTrue(expenseRepository.getByUserId("userid") instanceof ArrayList);
  }

  @Test
  void testExpenseSave() {
    when(expenseDao.saveExpense(
        eq("userid"),
        eq("name"),
        eq("category"),
        anyString(),
        eq("nominal")
    )).thenReturn(1);
    String description = "userid;name;category;nominal";
    Assertions.assertEquals(1, expenseRepository.saveExpense(description));
  }
}
