package com.bot.mymoney.repository;

import com.bot.mymoney.database.InvestmentDao;
import java.util.ArrayList;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class InvestmentRepositoryTest {
  @Mock
  private InvestmentDao investmentDao;

  @InjectMocks
  private InvestmentRepository investmentRepository;

  @Test
  void saveInvestmentTest() {
    when(investmentDao.saveInvestment(
        "12358AW",
        "LinusAW",
        "Kalkulator",
        "Bulan",
        "Awal",
        10000,
        4,
        2,
        5000,
        31845
    ))
        .thenReturn(1);

    investmentDao.saveInvestment(
        "12358AW",
        "LinusAW",
        "Kalkulator",
        "Bulan",
        "Awal",
        10000,
        4,
        2,
        5000,
        31845);
    verify(investmentDao).saveInvestment(
        "12358AW",
        "LinusAW",
        "Kalkulator",
        "Bulan",
        "Awal",
        10000,
        4,
        2,
        5000,
        31845);
  }

  @Test
  public void testInvestmentServiceGetAll() {
    when(investmentDao.get()).thenReturn(new ArrayList<>());
    Assertions.assertTrue(investmentRepository.getAll() instanceof ArrayList);
  }

  @Test
  void getByUserIdTest() {
    when(investmentDao.getByUserId("userId")).thenReturn(new ArrayList<>());
    Assertions.assertTrue(investmentRepository.getByUserId("userId") instanceof ArrayList);
  }
}
