package com.bot.mymoney.controller;

import com.bot.mymoney.model.Events;
import com.bot.mymoney.service.HandlingBot;
import com.bot.mymoney.service.ResponseBot;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.linecorp.bot.client.LineSignatureValidator;
import com.linecorp.bot.model.event.Event;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.ReplyEvent;
import com.linecorp.bot.model.objectmapper.ModelObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ControllerTest {
  @InjectMocks
  private Controller controller;

  @Mock
  private ResponseBot responseBot;

  @Mock
  private HandlingBot handlingBot;

  @Mock
  private LineSignatureValidator lineSignatureValidator;

  /**
   * Handle Event test.
   *
   * @throws JsonProcessingException
   */
  @Test
  void handleEventTest() throws JsonProcessingException {
    String eventsPayload = "{\"events\":[{\"type\":\"message\",\"replyToken\""
                               + ":\"c44827c4d26249d4be7b4e7da3a2c8f9\",\"source\":{\"userId\""
                               + ":\"Ue170feb3994bf25ad7adc3640564f75b\",\"type\":\"user\"},\""
                               + "timestamp\":1593088271679,\"mode\":\"active\",\"message\":"
                               + "{\"type\":\"text\",\"id\":\"12207939161791\","
                               + "\"text\":\"ASJDBJ\"}}],\"destination\""
                               + ":\"Udc4eaf8c8e5981d0fedfbf27fb1f1d51\"}";
    String lineSignature = "BnSbFgSAlKyUWgPPcTGoqPnOTsy5x5YE4a2BHQqAVys=";
    ObjectMapper objectMapper = ModelObjectMapper.createNewObjectMapper();
    Events eventsModel = objectMapper.readValue(eventsPayload, Events.class);
    Event event = eventsModel.getEvents().get(0);

    doNothing().when(handlingBot).handleMessageEvent((MessageEvent) event);
    controller.handleEvent(eventsPayload, lineSignature);
    verify(handlingBot).handleMessageEvent((MessageEvent) event);

    eventsPayload = "{\"events\":[{\"type\":\"follow\",\"replyToken\""
                        + ":\"408f3a41d18e4ccdbcf61dde3708187c\",\"source\":{\"userId\""
                        + ":\"Ue170feb3994bf25ad7adc3640564f75b\",\"type\":\"user\"},"
                        + "\"timestamp\":1593090013579,\"mode\":\"active\"}],\"destination\""
                        + ":\"Udc4eaf8c8e5981d0fedfbf27fb1f1d51\"}";
    lineSignature = "dEVWOiJPCb4aIUmLc1UxWANBQnS4u+ltX2p/1Hi/X5E=";
    objectMapper = ModelObjectMapper.createNewObjectMapper();
    eventsModel = objectMapper.readValue(eventsPayload, Events.class);
    event = eventsModel.getEvents().get(0);
    String replyToken = ((ReplyEvent) event).getReplyToken();
    doNothing().when(responseBot).greetingMessage(replyToken);
    controller.handleEvent(eventsPayload, lineSignature);
    verify(responseBot).greetingMessage(replyToken);
  }

  /**
   * Handle callback test.
   *
   * @throws JsonProcessingException
   */
  @Test
  void callbackTest() throws JsonProcessingException {
    String eventsPayload = "{\"events\":[{\"type\":\"message\",\"replyToken\""
                               + ":\"c44827c4d26249d4be7b4e7da3a2c8f9\",\"source\":{\"userId\""
                               + ":\"Ue170feb3994bf25ad7adc3640564f75b\",\"type\":\"user\"},"
                               + "\"timestamp\" :1593088271679,\"mode\":\"active\","
                               + "\"message\":{\"type\":\"text\",\"id\""
                               + ":\"12207939161791\",\"text\":\"ASJDBJ\"}}],\"destination\""
                               + ":\"Udc4eaf8c8e5981d0fedfbf27fb1f1d51\"}";
    String lineSignature = "BnSbFgSAlKyUWgPPcTGoqPnOTsy5x5YE4a2BHQqAVys=";

    when(lineSignatureValidator.validateSignature(eventsPayload.getBytes(),
        lineSignature)).thenReturn(false);
    controller.callback(lineSignature, eventsPayload);

    when(lineSignatureValidator.validateSignature(eventsPayload.getBytes(),
        lineSignature)).thenReturn(true);

    ObjectMapper objectMapper = ModelObjectMapper.createNewObjectMapper();
    Events eventsModel = objectMapper.readValue(eventsPayload, Events.class);
    Event event = eventsModel.getEvents().get(0);

    doNothing().when(handlingBot).handleMessageEvent((MessageEvent) event);
    controller.callback(lineSignature, eventsPayload);

    verify(lineSignatureValidator, times(2)).validateSignature(eventsPayload.getBytes(),
        lineSignature);
  }
}