package com.bot.mymoney.controller;

import com.bot.mymoney.service.laporan.LaporanServiceImp;
import net.minidev.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = LaporanController.class)
public class LaporanControllerTest {

  @Autowired
  private MockMvc mvc;

  @MockBean
  private LaporanServiceImp laporanService;

  /**
   * testControllerProcess.
   *
   * @throws Exception
   */
  @Test
  public void testControllerProcess() throws Exception {
    JSONObject json = new JSONObject();
    json.put("senderId", "userid");
    json.put("username", "username");
    json.put("message", "message");
    when(laporanService.handle("userid", "username", "message")).thenReturn("accepted");

    mvc.perform(post("/report")
                    .contentType(MediaType.APPLICATION_JSON).content(json.toString()))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$").value("accepted"));
  }
}