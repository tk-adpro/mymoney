package com.bot.mymoney.controller;

import com.bot.mymoney.model.InvestmentModel;
import com.bot.mymoney.service.investmentmanager.InvestmentServiceImpl;
import java.util.ArrayList;
import java.util.List;
import net.minidev.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = InvestmentController.class)
public class InvestmentControllerTest {

  @Autowired
  private MockMvc mvc;

  @MockBean
  private InvestmentServiceImpl investmentService;

  private InvestmentModel investmentModel;

  @BeforeEach
  public void setup() {
    investmentModel = new InvestmentModel(
        "12358AW",
        "LinusAW",
        "Kalkulator",
        "Bulan",
        "Awal",
        10000,
        4,
        2,
        5000,
        31845
    );
  }

  /**
   * testControllerGetAll.
   *
   * @throws Exception
   */
  @Test
  public void testControllerGetAll() throws Exception {
    List<InvestmentModel> list = new ArrayList<>();
    list.add(investmentModel);
    when(investmentService.getAll()).thenReturn(list);

    mvc.perform(get("/investment/get").contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$[0].category").value("Kalkulator"));
  }

  /**
   * testControllerGetByUserId.
   *
   * @throws Exception
   */
  @Test
  public void testControllerGetByUserId() throws Exception {
    List<InvestmentModel> list = new ArrayList<>();
    list.add(investmentModel);
    when(investmentService.getByUserId("userid")).thenReturn(list);

    mvc.perform(get("/investment/getbyuserid/userid").contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$[0].category").value("Kalkulator"));
  }

  /**
   * testControllerProcess.
   *
   * @throws Exception
   */
  @Test
  public void testControllerProcess() throws Exception {
    JSONObject json = new JSONObject();
    json.put("senderId", "userid");
    json.put("username", "username");
    json.put("message", "message");
    when(investmentService.handle("userid", "username", "message")).thenReturn("accepted");

    mvc.perform(post("/investment")
                    .contentType(MediaType.APPLICATION_JSON).content(json.toString()))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$").value("accepted"));
  }
}
