package com.bot.mymoney.service.record;

import com.bot.mymoney.model.Record;
import com.bot.mymoney.repository.RecordRepository;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.when;


@ExtendWith(MockitoExtension.class)
public class RecordServiceTest {

  @InjectMocks
  RecordServiceImp recordService;

  @Mock
  RecordRepository recordRepository;

  private Record record;

  @BeforeEach
  void setup() throws Exception {
    record = new Record(
        "userid",
        "name",
        "category",
        "timestamp",
        "50000"
    );
  }

  @Test
  public void testRecordServiceGetAll() {
    when(recordRepository.getAll()).thenReturn(new ArrayList<>());
    Assertions.assertTrue(recordService.getAll() instanceof ArrayList);
  }

  @Test
  public void testRecordServiceGetByUserId() {
    List<Record> list = new ArrayList<>();
    list.add(record);
    when(recordRepository.getByUserId("userid")).thenReturn(list);
    Assertions.assertTrue(recordService.getByUserId("userid").contains(record));
  }

  @Test
  public void testRecordServiceHandle() {
    Assertions.assertTrue(
        recordService.handle("userid", "username", "gaji")
            .contains("gaji"));
    Assertions.assertTrue(
        recordService.handle("userid", "username", "50000")
            .contains("50000"));
    Assertions.assertTrue(
        recordService.handle("userid", "username", "ya")
            .contains("berhasil dicatat"));
  }
}
