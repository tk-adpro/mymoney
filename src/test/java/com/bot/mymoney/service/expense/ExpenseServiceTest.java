package com.bot.mymoney.service.expense;

import com.bot.mymoney.model.Expense;
import com.bot.mymoney.repository.ExpenseRepository;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ExpenseServiceTest {

  @Mock
  ExpenseRepository expenseRepository;

  @InjectMocks
  ExpenseServiceImp expenseService;

  private Expense expense;

  @BeforeEach
  void setup() throws Exception {
    expense = new Expense(
        "userid",
        "name",
        "category",
        "timestamp",
        "10000"
    );
  }

  @Test
  public void testExpenseServiceGetAll() {
    when(expenseRepository.getAll()).thenReturn(new ArrayList<>());

    Assertions.assertTrue(expenseService.getAll() instanceof ArrayList);
  }

  @Test
  public void testExpenseServiceGetByUserId() {
    List<Expense> list = new ArrayList<>();
    list.add(expense);
    when(expenseRepository.getByUserId("userid")).thenReturn(list);

    Assertions.assertTrue(expenseService.getByUserId("userid").contains(expense));
  }

  @Test
  public void testExpenseServiceHandle() {
    Assertions.assertTrue(
        expenseService.handle("userid", "username", "konsumsi")
            .contains("konsumsi"));
    Assertions.assertTrue(
        expenseService.handle("userid", "username", "20000")
            .contains("20000"));
    Assertions.assertTrue(
        expenseService.handle("userid", "username", "ya")
            .contains("berhasil dicatat"));
  }
}
