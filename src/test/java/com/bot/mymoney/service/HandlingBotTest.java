package com.bot.mymoney.service;

import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.linecorp.bot.model.event.source.Source;
import com.linecorp.bot.model.event.source.UserSource;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.profile.UserProfileResponse;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.time.Instant;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class HandlingBotTest {
  @InjectMocks
  HandlingBotImp handlingBot = new HandlingBotImp();
  @Mock
  MessageEvent messageEvent;
  @Mock
  Source source;
  @Mock
  UserProfileResponse userProfile;
  @Mock
  ResponseBotImp responseBot;
  @Mock
  FlexBotImp flexBot;
  @Mock
  FlexMessage flexMessage;
  private Class<?> handlingBotServiceClass;

  @BeforeEach
  public void setup() throws Exception {
    handlingBotServiceClass = Class.forName("com.bot.mymoney.service.HandlingBotImp");

    source = new Source() {
      @Override
      public String getUserId() {
        return "userid";
      }

      @Override
      public String getSenderId() {
        return "userid";
      }
    };

    messageEvent = new MessageEvent(
        "replytoken",
        new UserSource("userid"),
        new TextMessageContent("id", "menu"),
        Instant.now()
    );
  }

  @Test
  public void testHandlingBotHasHandleMessageEventMethod() throws Exception {
    Method handleMessageEvent = handlingBotServiceClass
                                    .getDeclaredMethod("handleMessageEvent", MessageEvent.class);
    int methodModifiers = handleMessageEvent.getModifiers();

    Assertions.assertTrue(Modifier.isPublic(methodModifiers));
    Assertions.assertEquals(1, handleMessageEvent.getParameterCount());
    Assertions.assertEquals("void", handleMessageEvent.getGenericReturnType().getTypeName());
  }

  @Test
  public void testHandlingBotHasHandleNullMessageMethod() throws Exception {
    Method handleNullMessage = handlingBotServiceClass
                                   .getDeclaredMethod("handleNullMessage");
    int methodModifiers = handleNullMessage.getModifiers();

    Assertions.assertTrue(Modifier.isPublic(methodModifiers));
    Assertions.assertEquals(0, handleNullMessage.getParameterCount());
    Assertions.assertEquals("void", handleNullMessage.getGenericReturnType().getTypeName());
  }

  @Test
  public void testHandleMessageEventMenu() {
    when(responseBot.getSource()).thenReturn(source);
    when(responseBot.getProfile("userid")).thenReturn(userProfile);
    when(flexBot.createFlexMenu()).thenReturn(flexMessage);
    handlingBot.handleMessageEvent(messageEvent);
    verify(responseBot).reply("replytoken", flexMessage);
  }

  @Test
  public void testHandleMessageEventCatatPendapatan() {

    messageEvent = new MessageEvent(
        "replytoken",
        new UserSource("userid"),
        new TextMessageContent("id", "catat pendapatan"),
        Instant.now()
    );

    when(responseBot.getSource()).thenReturn(source);
    when(responseBot.getProfile("userid")).thenReturn(userProfile);
    when(flexBot.createFlexCatatPendapatan()).thenReturn(flexMessage);
    handlingBot.handleMessageEvent(messageEvent);
    verify(responseBot).reply("replytoken", flexMessage);
  }

  @Test
  public void testHandleMessageEventCatatPengeluaran() {

    messageEvent = new MessageEvent(
        "replytoken",
        new UserSource("userid"),
        new TextMessageContent("id", "catat pengeluaran"),
        Instant.now()
    );

    when(responseBot.getSource()).thenReturn(source);
    when(responseBot.getProfile("userid")).thenReturn(userProfile);
    when(flexBot.createFlexCatatPengeluaran()).thenReturn(flexMessage);
    handlingBot.handleMessageEvent(messageEvent);
    verify(responseBot).reply("replytoken", flexMessage);
  }

  @Test
  public void testHandleMessageEventLaporan() {

    messageEvent = new MessageEvent(
        "replytoken",
        new UserSource("userid"),
        new TextMessageContent("id", "laporan"),
        Instant.now()
    );

    when(responseBot.getSource()).thenReturn(source);
    when(responseBot.getProfile("userid")).thenReturn(userProfile);
    when(flexBot.createFlexLaporan()).thenReturn(flexMessage);
    handlingBot.handleMessageEvent(messageEvent);
    verify(responseBot).reply("replytoken", flexMessage);
  }

  @Test
  public void testHandleMessageEventKalkulatorInvestasi() {

    messageEvent = new MessageEvent(
        "replytoken",
        new UserSource("userid"),
        new TextMessageContent("id", "kalkulator investasi"),
        Instant.now()
    );

    when(responseBot.getSource()).thenReturn(source);
    when(responseBot.getProfile("userid")).thenReturn(userProfile);
    handlingBot.handleMessageEvent(messageEvent);
    verify(responseBot).replyText("replytoken", "Kalkulator investasi telah dipilih.\n"
                                                    + "Mohon pilih pola contribusi "
                                                    + "|(awal/akhir) + (bulan/tahun)| Contoh: "
                                                    + "akhir tahun");
  }

  @Test
  public void testHandleMessageEventUnknown() {

    messageEvent = new MessageEvent(
        "replytoken",
        new UserSource("userid"),
        new TextMessageContent("id", "asd"),
        Instant.now()
    );

    when(responseBot.getSource()).thenReturn(source);
    when(responseBot.getProfile("userid")).thenReturn(userProfile);
    handlingBot.handleMessageEvent(messageEvent);

    String unknown = "Permintaan tidak dikenali. "
                         + "Ketik 'menu' untuk melihat daftar tindakan yang dapat dilakukan.";
    verify(responseBot).replyText("replytoken", unknown);
  }

  @Test
  public void testHandleNotNullMessageCancel() {

    messageEvent = new MessageEvent(
        "replytoken",
        new UserSource("userid"),
        new TextMessageContent("id", "menu"),
        Instant.now()
    );

    when(responseBot.getSource()).thenReturn(source);
    when(responseBot.getProfile("userid")).thenReturn(userProfile);
    when(flexBot.createFlexMenu()).thenReturn(flexMessage);
    handlingBot.handleMessageEvent(messageEvent);
    verify(responseBot).reply("replytoken", flexMessage);

    ResponseEntity<String> response = new ResponseEntity<>("dibatalkan", HttpStatus.OK);

    handlingBot.handleNotNullMessage(response);
    verify(responseBot).replyText("replytoken", "dibatalkan");
  }

  @Test
  public void testHandleNotNullMessage() {

    messageEvent = new MessageEvent(
        "replytoken",
        new UserSource("userid"),
        new TextMessageContent("id", "menu"),
        Instant.now()
    );

    when(responseBot.getSource()).thenReturn(source);
    when(responseBot.getProfile("userid")).thenReturn(userProfile);
    when(flexBot.createFlexMenu()).thenReturn(flexMessage);
    handlingBot.handleMessageEvent(messageEvent);
    verify(responseBot).reply("replytoken", flexMessage);

    ResponseEntity<String> response = new ResponseEntity<>("reply message", HttpStatus.OK);

    handlingBot.handleNotNullMessage(response);
    verify(responseBot).replyText("replytoken", "reply message");

    ResponseEntity<String> write_response = new ResponseEntity<>("berhasil dicatat", HttpStatus.OK);

    handlingBot.handleNotNullMessage(write_response);
    verify(responseBot).replyText("replytoken", "berhasil dicatat");

    ResponseEntity<String> fail_response = new ResponseEntity<>("reply message", HttpStatus.BAD_GATEWAY);

    handlingBot.handleNotNullMessage(fail_response);
    verify(responseBot).replyText(
        "replytoken",
        "Maaf, fitur ini sedang tidak aktif. Harap kembali ke menu.");
  }
}
