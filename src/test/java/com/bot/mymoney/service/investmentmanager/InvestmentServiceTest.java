package com.bot.mymoney.service.investmentmanager;

import com.bot.mymoney.model.InvestmentModel;
import com.bot.mymoney.repository.InvestmentRepository;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.when;


@ExtendWith(MockitoExtension.class)
public class InvestmentServiceTest {

  @InjectMocks
  InvestmentServiceImpl investmentService;

  @Mock
  InvestmentRepository investmentRepository;

  private InvestmentModel investmentModel;

  @BeforeEach
  void setup() throws Exception {
    investmentModel = new InvestmentModel(
        "12358AW",
        "LinusAW",
        "Kalkulator",
        "Bulan",
        "Awal",
        10000,
        4,
        2,
        5000,
        31845
    );
  }

  @Test
  public void testInvestmentServiceGetAll() {
    when(investmentRepository.getAll()).thenReturn(new ArrayList<>());
    Assertions.assertTrue(investmentService.getAll() instanceof ArrayList);
  }

  @Test
  public void testInvestmentServiceGetByUserId() {
    List<InvestmentModel> list = new ArrayList<>();
    list.add(investmentModel);
    when(investmentRepository.getByUserId("userid")).thenReturn(list);
    Assertions.assertTrue(investmentService.getByUserId("userid").contains(investmentModel));
  }

  @Test
  public void testInvestmentServiceHandle() { //TODO: Enhance test
    Assertions.assertTrue(
        investmentService.handle("userid", "username", "kalkulator")
            .contains("kalkulator"));
  }
}
