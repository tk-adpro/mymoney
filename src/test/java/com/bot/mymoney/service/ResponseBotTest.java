package com.bot.mymoney.service;

import com.bot.mymoney.repository.UserRepository;
import com.linecorp.bot.client.LineMessagingClient;
import com.linecorp.bot.model.ReplyMessage;
import com.linecorp.bot.model.event.source.Source;
import com.linecorp.bot.model.message.TextMessage;
import com.linecorp.bot.model.profile.UserProfileResponse;
import com.linecorp.bot.model.response.BotApiResponse;
import java.util.Collections;
import java.util.concurrent.CompletableFuture;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ResponseBotTest {

  @InjectMocks
  ResponseBotImp responseBot;

  @Mock
  LineMessagingClient lineMessagingClient;

  @Mock
  UserRepository userRepository;

  @Mock
  Source source;

  @BeforeEach
  public void setup() {
    Source source = new Source() {
      @Override
      public String getUserId() {
        return "userid";
      }

      @Override
      public String getSenderId() {
        return "senderid";
      }
    };
  }

  @Test
  public void testResponseBotSetAndGetSource() {
    responseBot.setSource(source);
    Assertions.assertEquals(source, responseBot.getSource());
  }

  @Test
  public void testRespnoseBotGreetingsMessage() {
    when(lineMessagingClient.getProfile(null))
        .thenReturn(CompletableFuture.completedFuture(
            new UserProfileResponse(
                "username",
                "userid",
                "pictureurl",
                "status")
        ));
    when(lineMessagingClient.replyMessage(new ReplyMessage("replytoken", new TextMessage("message"))))
        .thenReturn(CompletableFuture.completedFuture(new BotApiResponse("ok", Collections.EMPTY_LIST)));
    responseBot.greetingMessage("replytoken");
    verify(lineMessagingClient)
        .replyMessage(new ReplyMessage("replytoken", new TextMessage("Selamat datang!")));
  }

  @Test
  public void testResponseBotGetProfile() {
    UserProfileResponse userProfileResponse = new UserProfileResponse(
        "username",
        "userid",
        "pictureurl",
        "status"
    );
    when(lineMessagingClient.getProfile("userid"))
        .thenReturn(CompletableFuture.completedFuture(userProfileResponse));
    Assertions.assertEquals(userProfileResponse, responseBot.getProfile("userid"));

    when(lineMessagingClient.getProfile("userid2"))
        .thenThrow(new RuntimeException());
    Assertions.assertNull(responseBot.getProfile(null));
  }
}
