package com.bot.mymoney.service.laporan;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class LaporanServiceTest {
  private LaporanServiceImp laporanService;

  @BeforeEach
  void setup() {
    laporanService = new LaporanServiceImp();
  }

  @Test
  public void testLaporanServiceHandle() {
    Assertions.assertTrue(
        laporanService.handle("userid", "username", "laporan pengeluaran")
            .contains("laporan pengeluaran"));
  }
}


