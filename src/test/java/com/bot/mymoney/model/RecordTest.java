package com.bot.mymoney.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class RecordTest {
  Record record;

  @BeforeEach
  void setUp() {
    record = new Record(
        "userId",
        "luthfi",
        "usaha",
        "timestamp",
        "50000"
    );
  }

  /**
   * Getter test.
   **/
  @Test
  void getUserIdTest() {
    String userId = record.getUserId();
    assertEquals(userId, "userId");
  }

  @Test
  void getNameTest() {
    String name = record.getName();
    assertEquals(name, "luthfi");
  }

  @Test
  void getCategoryTest() {
    String category = record.getCategory();
    assertEquals(category, "usaha");
  }

  @Test
  void getTimestampTest() {
    String timestamp = record.getTimestamp();
    assertEquals(timestamp, "timestamp");
  }

  @Test
  void getNominalTest() {
    String nominal = record.getNominal();
    assertEquals(nominal, "50000");
  }


  /**
   * Setter test.
   **/
  @Test
  void setUserIdTest() {
    record.setUserId("userId2");
    String userId = record.getUserId();
    assertEquals(userId, "userId2");
  }

  @Test
  void setNameTest() {
    record.setName("fahlevi");
    String name = record.getName();
    assertEquals(name, "fahlevi");
  }

  @Test
  void setCategoryTest() {
    record.setCategory("gaji");
    String category = record.getCategory();
    assertEquals(category, "gaji");
  }

  @Test
  void setTimestampTest() {
    record.setTimestamp("timestamp2");
    String timestamp = record.getTimestamp();
    assertEquals(timestamp, "timestamp2");
  }

  @Test
  void setNominalTest() {
    record.setNominal("100000");
    String nominal = record.getNominal();
    assertEquals(nominal, "100000");
  }
}
