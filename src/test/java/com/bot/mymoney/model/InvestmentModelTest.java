package com.bot.mymoney.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class InvestmentModelTest {
  InvestmentModel investmentModel;

  @BeforeEach
  void setUp() {
    investmentModel = new InvestmentModel(
        "12358AW",
        "LinusAW",
        "Kalkulator",
        "Bulan",
        "Awal",
        10000,
        4,
        2,
        5000,
        31845
    );
  }

  /**
   * Getter test.
   */
  @Test
  void getUserIdTest() {
    String userId = investmentModel.getUserId();
    assertEquals(userId, "12358AW");
  }

  /**
   * Getter test.
   */
  @Test
  void getNameTest() {
    String name = investmentModel.getUsername();
    assertEquals(name, "LinusAW");
  }

  /**
   * Getter test.
   */
  @Test
  void getCategoryTest() {
    String category = investmentModel.getCategory();
    assertEquals(category, "Kalkulator");
  }

  /**
   * Getter test.
   */
  @Test
  void getTimeSpanTest() {
    String timeSpan = investmentModel.getTimeSpan();
    assertEquals(timeSpan, "Bulan");
  }

  /**
   * Getter test.
   */
  @Test
  void getModeTest() {
    String timestamp = investmentModel.getMode();
    assertEquals(timestamp, "Awal");
  }

  /**
   * Getter test.
   */
  @Test
  void getFundsTest() {
    int nominal = investmentModel.getFunds();
    assertEquals(nominal, 10000);
  }

  /**
   * Getter test.
   */
  @Test
  void getTimeTest() {
    int nominal = investmentModel.getTime();
    assertEquals(nominal, 4);
  }

  /**
   * Getter test.
   */
  @Test
  void getRateTest() {
    int nominal = investmentModel.getRate();
    assertEquals(nominal, 2);
  }

  /**
   * Getter test.
   */
  @Test
  void getContribTest() {
    int nominal = investmentModel.getContrib();
    assertEquals(nominal, 5000);
  }

  /**
   * Getter test.
   */
  @Test
  void getResultTest() {
    int nominal = investmentModel.getResult();
    assertEquals(nominal, 31845);
  }

  /**
   * Setter test.
   **/
  @Test
  void setUserIdTest() {
    investmentModel.setUserId("userId2");
    String userId = investmentModel.getUserId();
    assertEquals(userId, "userId2");
  }

  /**
   * Setter test.
   */
  @Test
  void setNameTest() {
    investmentModel.setUsername("LAbhyasa");
    String name = investmentModel.getUsername();
    assertEquals(name, "LAbhyasa");
  }

  /**
   * Setter test.
   */
  @Test
  void setCategoryTest() {
    investmentModel.setCategory("Bukan Kalkulator");
    String category = investmentModel.getCategory();
    assertEquals(category, "Bukan Kalkulator");
  }

  /**
   * Setter test.
   */
  @Test
  void setTimestampTest() {
    investmentModel.setTimeSpan("Tahun");
    String timeSpan = investmentModel.getTimeSpan();
    assertEquals(timeSpan, "Tahun");
  }

  /**
   * Setter test.
   */
  @Test
  void setModeTest() {
    investmentModel.setMode("Akhir");
    String timestamp = investmentModel.getMode();
    assertEquals(timestamp, "Akhir");
  }

  /**
   * Setter test.
   */
  @Test
  void setFundsTest() {
    investmentModel.setFunds(50000);
    int nominal = investmentModel.getFunds();
    assertEquals(nominal, 50000);
  }

  /**
   * Setter test.
   */
  @Test
  void setTimeTest() {
    investmentModel.setTime(10);
    int nominal = investmentModel.getTime();
    assertEquals(nominal, 10);
  }

  /**
   * Setter test.
   */
  @Test
  void setRateTest() {
    investmentModel.setRate(4);
    int nominal = investmentModel.getRate();
    assertEquals(nominal, 4);
  }

  /**
   * Setter test.
   */
  @Test
  void setContribTest() {
    investmentModel.setContrib(10000);
    int nominal = investmentModel.getContrib();
    assertEquals(nominal, 10000);
  }

  /**
   * Setter test.
   */
  @Test
  void setResultTest() {
    investmentModel.setResult(500000);
    int nominal = investmentModel.getResult();
    assertEquals(nominal, 500000);
  }
}
