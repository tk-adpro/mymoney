package com.bot.mymoney.model;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class ExpenseTest {

  private Expense expense;

  @BeforeEach
  void setup() throws Exception {
    expense = new Expense(
        "userid",
        "name",
        "category",
        "timestamp",
        "10000"
    );
  }

  @Test
  void testExpenseGetUserId() {
    Assertions.assertEquals("userid", expense.getUserId());
  }

  @Test
  void testExpenseSetUserId() {
    Assertions.assertEquals("userid", expense.getUserId());
    expense.setUserId("newuserid");
    Assertions.assertEquals("newuserid", expense.getUserId());
  }

  @Test
  void testExpenseGetTitle() {
    Assertions.assertEquals("name", expense.getName());
  }

  @Test
  void testExpenseSetTitle() {
    Assertions.assertEquals("name", expense.getName());
    expense.setName("newname");
    Assertions.assertEquals("newname", expense.getName());
  }

  @Test
  void testExpenseGetCategory() {
    Assertions.assertEquals("category", expense.getCategory());
  }

  @Test
  void testExpenseSetCategory() {
    Assertions.assertEquals("category", expense.getCategory());
    expense.setCategory("newcategory");
    Assertions.assertEquals("newcategory", expense.getCategory());
  }

  @Test
  void testExpenseGetTimestamp() {
    Assertions.assertEquals("timestamp", expense.getTimestamp());
  }

  @Test
  void testExpenseSetTimestamp() {
    Assertions.assertEquals("timestamp", expense.getTimestamp());
    expense.setTimestamp("newtimestamp");
    Assertions.assertEquals("newtimestamp", expense.getTimestamp());
  }

  @Test
  void testExpenseGetNominal() {
    Assertions.assertEquals("10000", expense.getNominal());
  }

  @Test
  void testExpenseSetNominal() {
    Assertions.assertEquals("10000", expense.getNominal());
    expense.setNominal("20000");
    Assertions.assertEquals("20000", expense.getNominal());
  }
}
