package com.bot.mymoney.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class UserTest {
  User user;

  @BeforeEach
  void setUp() {
    user = new User(
        "userId",
        "luthfi",
        "timestamp"
    );
  }

  /**
   * Getter.
   **/
  @Test
  void getUserIdTest() {
    String userId = user.getUserId();
    assertEquals(userId, "userId");
  }

  @Test
  void getNameTest() {
    String name = user.getName();
    assertEquals(name, "luthfi");
  }

  @Test
  void getReminderTest() {
    String timestamp = user.getReminder();
    assertEquals(timestamp, "timestamp");
  }

  /**
   * Setter.
   **/
  @Test
  void setUserIdTest() {
    user.setUserId("userId2");
    String userId = user.getUserId();
    assertEquals(userId, "userId2");
  }

  @Test
  void setNameTest() {
    user.setName("fahlevi");
    String name = user.getName();
    assertEquals(name, "fahlevi");
  }

  @Test
  void setReminderTest() {
    user.setReminder("timestamp2");
    String timestamp = user.getReminder();
    assertEquals(timestamp, "timestamp2");
  }
}
