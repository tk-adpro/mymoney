package com.bot.mymoney.database;

import com.bot.mymoney.model.Expense;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import javax.sql.DataSource;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ExpenseDaoTest {
  @InjectMocks
  ExpenseDaoImpl expenseDao = new ExpenseDaoImpl(getDataSource());
  @Mock
  private JdbcTemplate jdbcTemplate;
  @Mock
  private ResultSet resultSet;

  DataSource getDataSource() {
    String dbUrl = System.getenv("JDBC_DATABASE_URL");
    String username = System.getenv("JDBC_DATABASE_USERNAME");
    String password = System.getenv("JDBC_DATABASE_PASSWORD");
    DriverManagerDataSource dataSource = new DriverManagerDataSource();

    dataSource.setDriverClassName("org.postgresql.Driver");
    dataSource.setUrl(dbUrl);
    dataSource.setUsername(username);
    dataSource.setPassword(password);

    return dataSource;
  }

  @Test
  void getTest() {
    List<Expense> expenses = expenseDao.get();
    Assertions.assertNull(expenses);
  }

  @Test
  void getByUserIdTest() {
    List<Expense> expenses = expenseDao.getByUserId("userId");
    Assertions.assertNull(expenses);
  }

  @Test
  void saveRecordTest() {
    int status = expenseDao.saveExpense("userId", "name", "category",
        "timestamp", "nominal");
    Assertions.assertEquals(status, 0);
  }

  @Test
  void extractDataTest() throws SQLException {
    when(resultSet.next()).thenReturn(true).thenReturn(false);
    List<Expense> expenses = ExpenseDaoImpl.extractData(resultSet);
    Assertions.assertNotNull(expenses);
  }
}
