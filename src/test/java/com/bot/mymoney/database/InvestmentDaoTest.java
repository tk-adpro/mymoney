package com.bot.mymoney.database;

import com.bot.mymoney.model.InvestmentModel;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import javax.sql.DataSource;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class InvestmentDaoTest {
  @InjectMocks
  InvestmentDaoImpl investmentDao = new InvestmentDaoImpl(getDataSource());
  @Mock
  private JdbcTemplate jdbcTemplate;
  @Mock
  private ResultSet resultSet;

  DataSource getDataSource() {
    String dbUrl = System.getenv("JDBC_DATABASE_URL");
    String username = System.getenv("JDBC_DATABASE_USERNAME");
    String password = System.getenv("JDBC_DATABASE_PASSWORD");
    DriverManagerDataSource dataSource = new DriverManagerDataSource();

    dataSource.setDriverClassName("org.postgresql.Driver");
    dataSource.setUrl(dbUrl);
    dataSource.setUsername(username);
    dataSource.setPassword(password);

    return dataSource;
  }

  @Test
  void getTest() {
    List<InvestmentModel> investments = investmentDao.get();
    Assertions.assertNull(investments);
  }

  @Test
  void getByUserIdTest() {
    List<InvestmentModel> investments = investmentDao.getByUserId("userId");
    Assertions.assertNull(investments);
  }

  @Test
  void saveRecordTest() {
    int investments = investmentDao.saveInvestment(
        "12358AW",
        "LinusAW",
        "Kalkulator",
        "Bulan",
        "Awal",
        10000,
        4,
        2,
        5000,
        31845
    );
    Assertions.assertEquals(investments, 0);
  }

  @Test
  void extractDataTest() throws SQLException {
    when(resultSet.next()).thenReturn(true).thenReturn(false);
    List<InvestmentModel> records = InvestmentDaoImpl.extractData(resultSet);
    Assertions.assertNotNull(records);
  }
}