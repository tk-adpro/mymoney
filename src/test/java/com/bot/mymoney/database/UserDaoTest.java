package com.bot.mymoney.database;

import com.bot.mymoney.model.User;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import javax.sql.DataSource;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class UserDaoTest {
  @InjectMocks
  UserDaoImp userDao = new UserDaoImp(getDataSource());
  @Mock
  private JdbcTemplate jdbcTemplate;
  @Mock
  private ResultSet resultSet;

  DataSource getDataSource() {
    String dbUrl = System.getenv("JDBC_DATABASE_URL");
    String username = System.getenv("JDBC_DATABASE_USERNAME");
    String password = System.getenv("JDBC_DATABASE_PASSWORD");
    DriverManagerDataSource dataSource = new DriverManagerDataSource();

    dataSource.setDriverClassName("org.postgresql.Driver");
    dataSource.setUrl(dbUrl);
    dataSource.setUsername(username);
    dataSource.setPassword(password);

    return dataSource;
  }

  @Test
  void getTest() {
    List<User> users = userDao.get();
    Assertions.assertNull(users);
  }

  @Test
  void getByUserIdTest() {
    List<User> users = userDao.getByUserId("userId");
    Assertions.assertNull(users);
  }

  @Test
  void registerUserTest() {
    int user = userDao.registerUser("userId", "displayName");
    Assertions.assertEquals(user, 0);
  }

  @Test
  void statusNotifikasiByUserIdTest() {
    userDao.setStatusNotifikasiByUserId("true", "userId");
    List<User> userList = userDao.getStatusNotifikasiByUserId("userId");
    Assertions.assertNull(userList);
    userList = userDao.getAllUserNotifikasiAktif();
    Assertions.assertNull(userList);
  }

  @Test
  void extractDataTest() throws SQLException {
    when(resultSet.next()).thenReturn(true).thenReturn(false);
    List<User> users = UserDaoImp.extractData(resultSet);
    Assertions.assertNotNull(users);
  }
}
