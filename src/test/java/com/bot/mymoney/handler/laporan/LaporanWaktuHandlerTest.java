package com.bot.mymoney.handler.laporan;

import com.bot.mymoney.model.Expense;
import com.bot.mymoney.model.Record;
import java.util.List;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class LaporanWaktuHandlerTest {
  private LaporanWaktuHandler currentState;
  private LaporanWaktuHandler currentState2;
  private Expense expense;
  private List<Expense> expenseList;
  private Record record;
  private List<Record> recordList;


  @BeforeEach
  void setup() {
    LaporanCatatanHandler oldState = new LaporanCatatanHandler("userid", "Anan");
    LaporanCatatanHandler oldState2 = new LaporanCatatanHandler("useridB", "Kevin");
    currentState = (LaporanWaktuHandler) oldState.verificationMessage("laporan pengeluaran");
    currentState2 = (LaporanWaktuHandler) oldState2.verificationMessage("laporan pendapatan");

  }

  @Test
  void testChooseCategoryPengeluaran() {
    LaporanHandler nextState = currentState.verificationMessage("harian");
    Assertions.assertNull(nextState);
  }

  @Test
  void testChooseCategoryPendapatan() {
    LaporanHandler nextState = currentState2.verificationMessage("mingguan");
    Assertions.assertNull(nextState);
  }

  @Test
  void testStringToDayBulanan() {
    LaporanHandler nextState = currentState2.verificationMessage("bulanan");
    Assertions.assertNull(nextState);
  }

  @Test
  void testChooseWrongCategory() {
    LaporanHandler nextState = currentState.verificationMessage("Intentional wrong input");
    Assertions.assertTrue(nextState instanceof LaporanWaktuHandler);
  }

  @Test
  void testCancelOperation() {
    LaporanHandler nextState = currentState.verificationMessage("batal");
    Assertions.assertNull(nextState);
  }

  @Test
  void testGetDescription() {
    Assertions.assertEquals("userid;Anan;laporan pengeluaran", currentState.getDescription());
  }
}
