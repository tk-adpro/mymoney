package com.bot.mymoney.handler.laporan;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class LaporanCatatanHandlerTest {
  private LaporanCatatanHandler currentState;

  @BeforeEach
  void setup() {
    currentState = new LaporanCatatanHandler("userid", "Anan");
  }

  @Test
  void testChooseCategory() {
    LaporanHandler nextState = currentState.verificationMessage("laporan pengeluaran");
    Assertions.assertTrue(nextState instanceof LaporanWaktuHandler);
  }

  @Test
  void testChooseWrongCategory() {
    LaporanHandler nextState = currentState.verificationMessage("Intentional wrong input");
    Assertions.assertTrue(nextState instanceof LaporanCatatanHandler);
  }

  @Test
  void testCancelOperation() {
    LaporanHandler nextState = currentState.verificationMessage("batal");
    Assertions.assertNull(nextState);
  }

  @Test
  void testGetDescription() {
    Assertions.assertEquals("userid;Anan", currentState.getDescription());
  }
}

