package com.bot.mymoney.handler.record;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class PendapatanKonfirmasiHandlerTest {
  final PendapatanHandler currentHandler = new PendapatanKonfirmasiHandler(
      new PendapatanNominalHandler(
          new PendapatanKategoriHandler("senderId", "name"))
  );

  @Test
  void verificationMessageTrueTest() {
    PendapatanTemplate nextHandler = this.currentHandler.verificationMessage("ya");
    assertNull(nextHandler);
  }

  @Test
  void verificationMessageBatalTest() {
    PendapatanTemplate nextHandler = this.currentHandler.verificationMessage("batal");
    assertNull(nextHandler);
    PendapatanTemplate handler = (PendapatanTemplate) currentHandler;
    assertEquals("Proses fitur dibatalkan", handler.getMessageToUser());
  }

  @Test
  void verificationMessageUnknownTest() {
    PendapatanTemplate nextHandler = this.currentHandler.verificationMessage("hai");
    assertTrue(nextHandler instanceof PendapatanKonfirmasiHandler);
  }

  @Test
  void getDescription() {
    String description = currentHandler.getDescription();
    assertEquals("senderId;name;null;null", description);
  }
}
