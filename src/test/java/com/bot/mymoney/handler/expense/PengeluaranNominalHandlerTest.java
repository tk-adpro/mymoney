package com.bot.mymoney.handler.expense;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class PengeluaranNominalHandlerTest {

  private PengeluaranNominalHandler currentState;

  @BeforeEach
  void setup() {
    PengeluaranKategoriHandler oldState = new PengeluaranKategoriHandler("userid", "username");
    currentState = (PengeluaranNominalHandler) oldState.verificationMessage("konsumsi");
  }

  @Test
  void testInputValidNominal() {
    PengeluaranHandler nextState = currentState.verificationMessage("10000");
    Assertions.assertTrue(nextState instanceof PengeluaranKonfirmasiHandler);
  }

  @Test
  void testInputNotNumericalNominal() {
    PengeluaranHandler nextState = currentState.verificationMessage("mantap");
    Assertions.assertTrue(nextState instanceof PengeluaranNominalHandler);
  }

  @Test
  void testInputBelowZeroNominal() {
    PengeluaranHandler nextState = currentState.verificationMessage("-5");
    Assertions.assertTrue(nextState instanceof PengeluaranNominalHandler);
  }

  @Test
  void testCancelOperation() {
    PengeluaranHandler nextState = currentState.verificationMessage("batal");
    Assertions.assertNull(nextState);
    Assertions.assertEquals("Proses fitur dibatalkan", currentState.getMessageToUser());
  }

  @Test
  void testGetDescription() {
    Assertions.assertEquals("userid;username;konsumsi;", currentState.getDescription());
    PengeluaranKonfirmasiHandler nextState = (PengeluaranKonfirmasiHandler) currentState.verificationMessage("10000");
    Assertions.assertEquals("userid;username;konsumsi;10000", nextState.getDescription());
  }
}
