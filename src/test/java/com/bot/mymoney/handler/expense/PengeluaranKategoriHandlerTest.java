package com.bot.mymoney.handler.expense;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class PengeluaranKategoriHandlerTest {

  private PengeluaranKategoriHandler currentState;

  @BeforeEach
  void setup() {
    currentState = new PengeluaranKategoriHandler("userid", "username");
  }

  @Test
  void testChooseCategory() {
    PengeluaranHandler nextState = currentState.verificationMessage("konsumsi");
    Assertions.assertTrue(nextState instanceof PengeluaranNominalHandler);
  }

  @Test
  void testChooseWrongCategory() {
    PengeluaranHandler nextState = currentState.verificationMessage("mantap");
    Assertions.assertTrue(nextState instanceof PengeluaranKategoriHandler);
  }

  @Test
  void testCancelOperation() {
    PengeluaranHandler nextState = currentState.verificationMessage("batal");
    Assertions.assertNull(nextState);
    Assertions.assertEquals("Proses fitur dibatalkan", currentState.getMessageToUser());
  }

  @Test
  void testGetDescription() {
    Assertions.assertEquals("userid;username", currentState.getDescription());
  }
}
