package com.bot.mymoney.handler.expense;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class PengeluaranKonfirmasiHandlerTest {

  private PengeluaranKonfirmasiHandler currentState;

  @BeforeEach
  void setup() {
    PengeluaranKategoriHandler firstState = new PengeluaranKategoriHandler("userid", "username");
    PengeluaranNominalHandler secondState = (PengeluaranNominalHandler)
                                                firstState.verificationMessage("konsumsi");
    currentState = (PengeluaranKonfirmasiHandler) secondState.verificationMessage("10000");
  }

  @Test
  void testKonfirmasiBerhasil() {
    PengeluaranHandler nextState = currentState.verificationMessage("ya");
    Assertions.assertNull(nextState);
  }

  @Test
  void testInputNonvalidNominal() {
    PengeluaranHandler nextState = currentState.verificationMessage("mantap");
    Assertions.assertTrue(nextState instanceof PengeluaranKonfirmasiHandler);
  }

  @Test
  void testCancelOperation() {
    PengeluaranHandler nextState = currentState.verificationMessage("batal");
    Assertions.assertNull(nextState);
    Assertions.assertEquals("Proses fitur dibatalkan", currentState.getMessageToUser());
  }

  @Test
  void testGetDescription() {
    Assertions.assertEquals("userid;username;konsumsi;10000", currentState.getDescription());
  }
}
