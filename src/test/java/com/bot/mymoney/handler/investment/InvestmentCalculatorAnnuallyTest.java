package com.bot.mymoney.handler.investment;

import com.bot.mymoney.handler.investment.calculator.InvestmentCalculator;
import com.bot.mymoney.handler.investment.calculator.InvestmentCalculatorAnnually;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class InvestmentCalculatorAnnuallyTest {
  final InvestmentCalculator calculator = new InvestmentCalculatorAnnually();

  @BeforeEach
  void setup() {
    calculator.input("funds", 500);
    calculator.input("time", 4);
    calculator.input("rate", 50);
    calculator.input("contrib", 500);
  }

  @Test
  public void calculateInvestmentStart() {
    calculator.setMode(0);
    calculator.calculateInvestment();
    Assertions.assertEquals(8625, calculator.getResult());
  }

  @Test
  public void calculateInvestmentEnd() {
    calculator.setMode(1);
    calculator.calculateInvestment();
    Assertions.assertEquals(6593, calculator.getResult());
  }
}
