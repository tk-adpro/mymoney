package com.bot.mymoney.handler.investment;

import com.bot.mymoney.handler.investment.calculator.InvestmentCalculator;
import com.bot.mymoney.handler.investment.calculator.InvestmentCalculatorMonthly;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class CalculatorHandlerContribTest {
  private InvestmentHandler investmentModel;

  @BeforeEach
  void setup() throws Exception {
    InvestmentCalculator calculator = new InvestmentCalculatorMonthly();
    calculator.input("funds", 500);
    calculator.input("time", 20);
    calculator.input("rate", 50);

    investmentModel = new CalculatorHandlerContrib(
        "12358AW",
        "LinusAW",
        calculator
    );
  }

  @Test
  public void testHandleCorrect() {
    InvestmentHandler nextHandler = investmentModel.verificationMessage("500");
    Assertions.assertTrue(nextHandler instanceof CalculatorHandlerConfirm);
  }

  @Test
  public void testHandleCancel() {
    InvestmentHandler nextHandler = investmentModel.verificationMessage("Batal");
    Assertions.assertTrue(nextHandler instanceof StateHandlerCancel);
  }

  @Test
  public void testHandleFail() {
    InvestmentHandler nextHandler = investmentModel.verificationMessage("Unknown");
    Assertions.assertTrue(nextHandler instanceof StateHandlerFail);
  }
}
