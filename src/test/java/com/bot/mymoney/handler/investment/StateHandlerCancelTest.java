package com.bot.mymoney.handler.investment;

import com.bot.mymoney.handler.investment.calculator.InvestmentCalculator;
import com.bot.mymoney.handler.investment.calculator.InvestmentCalculatorMonthly;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class StateHandlerCancelTest {
  private InvestmentHandler investmentModel;

  @BeforeEach
  void setup() throws Exception {
    InvestmentCalculator calculator = new InvestmentCalculatorMonthly();
    calculator.input("funds", 500);

    investmentModel = new StateHandlerCancel(
        "12358AW",
        "LinusAW"
    );
  }

  @Test
  public void testHandle() {
    InvestmentHandler nextHandler = investmentModel.verificationMessage("50");
    Assertions.assertNull(nextHandler);
  }
}
