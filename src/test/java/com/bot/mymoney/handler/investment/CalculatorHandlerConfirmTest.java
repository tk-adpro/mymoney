package com.bot.mymoney.handler.investment;

import com.bot.mymoney.handler.investment.calculator.InvestmentCalculator;
import com.bot.mymoney.handler.investment.calculator.InvestmentCalculatorMonthly;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class CalculatorHandlerConfirmTest {
  private InvestmentHandler investmentModel;

  @BeforeEach
  void setup() throws Exception {
    InvestmentCalculator calculator = new InvestmentCalculatorMonthly();
    calculator.input("funds", 500);
    calculator.input("time", 20);
    calculator.input("rate", 50);
    calculator.input("contrib", 500);

    investmentModel = new CalculatorHandlerConfirm(
        "12358AW",
        "LinusAW",
        calculator
    );
  }

  @Test
  public void testHandleCorrect() {
    InvestmentHandler nextHandler = investmentModel.verificationMessage("ya");
    Assertions.assertTrue(nextHandler instanceof StateHandlerSuccess);
  }

  @Test
  public void testHandleCancel() {
    InvestmentHandler nextHandler = investmentModel.verificationMessage("Batal");
    Assertions.assertTrue(nextHandler instanceof StateHandlerCancel);
  }

  @Test
  public void testHandleFail() {
    InvestmentHandler nextHandler = investmentModel.verificationMessage("Unknown");
    Assertions.assertTrue(nextHandler instanceof StateHandlerFail);
  }
}
