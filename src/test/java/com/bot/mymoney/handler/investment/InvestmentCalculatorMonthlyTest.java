package com.bot.mymoney.handler.investment;

import com.bot.mymoney.handler.investment.calculator.InvestmentCalculator;
import com.bot.mymoney.handler.investment.calculator.InvestmentCalculatorMonthly;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class InvestmentCalculatorMonthlyTest {
  final InvestmentCalculator calculator = new InvestmentCalculatorMonthly();

  @BeforeEach
  void setup() {
    calculator.input("funds", 1000);
    calculator.input("time", 2);
    calculator.input("rate", 100);
    calculator.input("contrib", 1000);
  }

  @Test
  public void calculateInvestmentStart() {
    calculator.setMode(0);
    calculator.calculateInvestment();
    Assertions.assertEquals(76000, calculator.getResult());
  }

  @Test
  public void calculateInvestmentEnd() {
    calculator.setMode(1);
    calculator.calculateInvestment();
    Assertions.assertEquals(73000, calculator.getResult());
  }
}
