package com.bot.mymoney.handler.investment;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class CalculatorHandlerCategoryTest {
  private InvestmentHandler investmentModel;

  @BeforeEach
  void setup() throws Exception {
    investmentModel = new CalculatorHandlerCategory(
        "12358AW",
        "LinusAW"
    );
  }

  @Test
  public void testHandleCorrect() {
    InvestmentHandler nextHandler = investmentModel.verificationMessage("Awal Bulan");
    Assertions.assertTrue(nextHandler instanceof CalculatorHandlerFunds);
  }

  @Test
  public void testHandleCancel() {
    InvestmentHandler nextHandler = investmentModel.verificationMessage("Batal");
    Assertions.assertTrue(nextHandler instanceof StateHandlerCancel);
  }

  @Test
  public void testHandleFail() {
    InvestmentHandler nextHandler = investmentModel.verificationMessage("Unknown");
    Assertions.assertTrue(nextHandler instanceof StateHandlerFail);
  }
}
