package com.bot.mymoney;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Running all file application.
 **/
@SpringBootApplication
public class MymoneyApplication {

  public static void main(String[] args) {
    SpringApplication.run(MymoneyApplication.class, args);
  }
}
