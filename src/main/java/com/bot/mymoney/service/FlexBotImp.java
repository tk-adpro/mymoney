package com.bot.mymoney.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.flex.container.FlexContainer;
import com.linecorp.bot.model.objectmapper.ModelObjectMapper;
import java.util.Objects;
import org.apache.commons.io.IOUtils;
import org.springframework.stereotype.Service;

/**
 * Implemetation FlexMessage (picture clickable)
 * that related with JSON file in resources.
 **/
@Service
public class FlexBotImp implements FlexBot {

  /**
   * Display all feature.
   **/
  public FlexMessage createFlexMenu() {
    FlexMessage flexMessage = new FlexMessage("Menu", null);
    try {
      ClassLoader classLoader = getClass().getClassLoader();
      String flexTemplate = IOUtils.toString(
          classLoader.getResourceAsStream("menu.json"));

      ObjectMapper objectMapper = ModelObjectMapper.createNewObjectMapper();
      FlexContainer flexContainer = objectMapper.readValue(flexTemplate, FlexContainer.class);
      flexMessage = new FlexMessage("Menu", flexContainer);

    } catch (Exception e) {
      e.printStackTrace();
    }
    return flexMessage;
  }

  /**
   * Display all Catat Pendapatan's category.
   **/
  public FlexMessage createFlexCatatPendapatan() {
    FlexMessage flexMessage = new FlexMessage("Kategori Pendapatan", null);
    try {
      ClassLoader classLoader = getClass().getClassLoader();
      String flexTemplate = IOUtils.toString(
          classLoader.getResourceAsStream("catatPendapatan.json"));

      ObjectMapper objectMapper = ModelObjectMapper.createNewObjectMapper();
      FlexContainer flexContainer = objectMapper.readValue(flexTemplate, FlexContainer.class);
      flexMessage = new FlexMessage("Kategori Pendapatan", flexContainer);

    } catch (Exception e) {
      e.printStackTrace();
    }
    return flexMessage;
  }

  /**
   * Display all Catat Pengeluaran's category.
   **/
  public FlexMessage createFlexCatatPengeluaran() {
    FlexMessage flexMessage = new FlexMessage("Kategori Pengeluaran", null);
    try {
      ClassLoader classLoader = getClass().getClassLoader();
      String flexTemplate = IOUtils.toString(
          classLoader.getResourceAsStream("catatPengeluaran.json"));

      ObjectMapper objectMapper = ModelObjectMapper.createNewObjectMapper();
      FlexContainer flexContainer = objectMapper.readValue(flexTemplate, FlexContainer.class);
      flexMessage = new FlexMessage("Kategori Pengeluaran", flexContainer);

    } catch (Exception e) {
      e.printStackTrace();
    }
    return flexMessage;
  }

  /**
   * Display all Laporan's category.
   **/
  public FlexMessage createFlexLaporan() {
    FlexMessage flexMessage = new FlexMessage("Kategori Laporan", null);
    try {
      ClassLoader classLoader = getClass().getClassLoader();
      String flexTemplate = IOUtils.toString(Objects.requireNonNull(
          classLoader.getResourceAsStream("laporan.json")));

      ObjectMapper objectMapper = ModelObjectMapper.createNewObjectMapper();
      FlexContainer flexContainer = objectMapper.readValue(flexTemplate, FlexContainer.class);
      flexMessage = new FlexMessage("Kategori Laporan", flexContainer);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return flexMessage;
  }
}
