package com.bot.mymoney.service;

import com.linecorp.bot.model.event.source.Source;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.profile.UserProfileResponse;

/**
 * Focus for replaying message to user.
 **/
public interface ResponseBot {

  void greetingMessage(String replyToken);

  void replyFlexMenu(String replyToken);

  void replyText(String replyToken, String message);

  void reply(String replyToken, Message message);

  Source getSource();

  void setSource(Source source);

  UserProfileResponse getProfile(String userId);
}
