package com.bot.mymoney.service;

import com.linecorp.bot.model.message.FlexMessage;

/**
 * FlexMessage to display category selection in picture that clickable.
 **/
public interface FlexBot {

  FlexMessage createFlexMenu();

  FlexMessage createFlexCatatPendapatan();

  FlexMessage createFlexCatatPengeluaran();

  FlexMessage createFlexLaporan();
}
