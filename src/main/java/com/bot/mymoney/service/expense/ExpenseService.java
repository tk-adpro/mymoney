package com.bot.mymoney.service.expense;

import com.bot.mymoney.model.Expense;
import java.util.List;

/**
 * Service for ExpenseController.
 **/
public interface ExpenseService {
  List<Expense> getAll();

  List<Expense> getByUserId(String userId);

  String handle(String userId, String username, String message);
}
