package com.bot.mymoney.service.record;

import com.bot.mymoney.model.Record;
import java.util.List;

/**
 * Service for RecordController.
 **/
public interface RecordService {
  List<Record> getAll();

  List<Record> getByUserId(String userId);

  String handle(String userId, String username, String message);
}
