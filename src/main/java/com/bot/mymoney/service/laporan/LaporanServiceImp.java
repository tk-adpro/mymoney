package com.bot.mymoney.service.laporan;

import com.bot.mymoney.handler.laporan.LaporanCatatanHandler;
import com.bot.mymoney.handler.laporan.LaporanHandler;
import java.util.HashMap;
import org.springframework.stereotype.Service;

/**
 * LaporanService Implementation.
 **/
@Service
public class LaporanServiceImp implements LaporanService {

  private HashMap<String, LaporanHandler> currentHandler = new HashMap<>();

  @Override
  public String handle(String userId, String username, String message) {
    if (!currentHandler.containsKey(userId) || currentHandler.get(userId) == null) {
      currentHandler.put(userId, new LaporanCatatanHandler(userId, username));
    }
    LaporanHandler handler = currentHandler.get(userId);
    currentHandler.put(userId, handler.verificationMessage(message));

    return handler.getMessageToUser();
  }

}
