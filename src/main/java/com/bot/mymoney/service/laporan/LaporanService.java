package com.bot.mymoney.service.laporan;

/**
 * Service for ExpenseController.
 **/
public interface LaporanService {

  String handle(String userId, String username, String message);
}
