package com.bot.mymoney.service.investmentmanager;

import com.bot.mymoney.model.InvestmentModel;
import java.util.List;
import org.springframework.stereotype.Service;

/**
 * Service for InvestmentController.
 **/
@Service
public interface InvestmentService {
  List<InvestmentModel> getAll();

  List<InvestmentModel> getByUserId(String userId);

  String handle(String userId, String username, String message);
}
