package com.bot.mymoney.service.investmentmanager;

import com.bot.mymoney.handler.investment.CalculatorHandlerCategory;
import com.bot.mymoney.handler.investment.InvestmentHandler;
import com.bot.mymoney.handler.investment.StateHandlerCancel;
import com.bot.mymoney.handler.investment.StateHandlerSuccess;
import com.bot.mymoney.model.InvestmentModel;
import com.bot.mymoney.repository.InvestmentRepository;
import java.util.HashMap;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * InvestmentService Implementation.
 **/
@Service
public class InvestmentServiceImpl implements InvestmentService {
  @Autowired
  private InvestmentRepository investmentRepository;

  private HashMap<String, InvestmentHandler> currentHandler = new HashMap<>();

  @Override
  public List<InvestmentModel> getAll() {
    return investmentRepository.getAll();
  }

  @Override
  public List<InvestmentModel> getByUserId(String userId) {
    return investmentRepository.getByUserId(userId);
  }

  @Override
  public String handle(String userId, String username, String message) {
    if (!currentHandler.containsKey(userId) || currentHandler.get(userId) == null) {
      currentHandler.put(userId, new CalculatorHandlerCategory(userId, username));
      return "Pilih kategori kalkulator (Contoh: Awal Bulan, Akhir Tahun) atau ketik batal.";
    }

    InvestmentHandler handler = currentHandler.get(userId);
    try {
      if (handler instanceof StateHandlerCancel) {
        throw new InterruptedException("Operation Cancelled");
      }
      if (handler instanceof StateHandlerSuccess) {
        investmentRepository.saveInvestment(handler.getDescription());
      }
      currentHandler.put(userId, handler.verificationMessage(message));
    } catch (InterruptedException e) {
      currentHandler.put(userId, null);
    } catch (Exception e) {
      return "Operasi gagal.";
    }

    return handler.getMessageToUser();
  }
}
