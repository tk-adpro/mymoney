package com.bot.mymoney.service;

import com.bot.mymoney.repository.UserRepository;
import com.linecorp.bot.client.LineMessagingClient;
import com.linecorp.bot.model.ReplyMessage;
import com.linecorp.bot.model.event.source.Source;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;
import com.linecorp.bot.model.profile.UserProfileResponse;
import java.util.concurrent.ExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Implementation that how and what
 * are the appropriate replies for specific user input.
 **/
@Service
public class ResponseBotImp implements ResponseBot {
  private Source source;
  @Autowired
  private LineMessagingClient lineMessagingClient;
  @Autowired
  private UserRepository userRepository;

  /**
   * Process when user input first message.
   **/
  public void greetingMessage(String replyToken) {
    String senderId = source.getSenderId();
    UserProfileResponse sender = getProfile(senderId);
    userRepository.registerUser(sender.getUserId(), sender.getDisplayName());
    replyFlexMenu(replyToken);
  }

  /**
   * FlexMessage Menu to display feature selection.
   **/
  public void replyFlexMenu(String replyToken) {
    String senderId = source.getSenderId();
    UserProfileResponse sender = getProfile(senderId);
    replyText(replyToken, "Selamat datang!");
  }

  public void replyText(String replyToken, String message) {
    TextMessage textMessage = new TextMessage(message);
    reply(replyToken, textMessage);
  }

  /**
   * reply message for user.
   **/
  public void reply(String replyToken, Message message) {
    ReplyMessage replyMessage = new ReplyMessage(replyToken, message);
    try {
      lineMessagingClient.replyMessage(replyMessage).get();
    } catch (InterruptedException | ExecutionException | RuntimeException e) {
      e.printStackTrace();
    }
  }

  public Source getSource() {
    return this.source;
  }

  public void setSource(Source source) {
    this.source = source;
  }

  /**
   * Get user data.
   */
  public UserProfileResponse getProfile(String userId) {
    try {
      return lineMessagingClient.getProfile(userId).get();
    } catch (InterruptedException | ExecutionException | RuntimeException e) {
      e.printStackTrace();
      return null;
    }
  }
}
