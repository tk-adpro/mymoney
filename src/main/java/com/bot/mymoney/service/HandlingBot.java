package com.bot.mymoney.service;

import com.linecorp.bot.model.event.MessageEvent;
import org.springframework.http.ResponseEntity;

/**
 * Handling Message from user (controller).
 **/
public interface HandlingBot {
  void handleMessageEvent(MessageEvent messageEvent);

  void handleNullMessage();

  void handleNotNullMessage(ResponseEntity<String> response);

  ResponseEntity<String> postUserData();

  boolean checkNullCurrentPath();
}
