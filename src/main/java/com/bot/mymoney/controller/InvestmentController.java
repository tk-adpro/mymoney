package com.bot.mymoney.controller;

import com.bot.mymoney.model.InvestmentModel;
import com.bot.mymoney.service.investmentmanager.InvestmentService;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller for Investment API access.
 **/
@RestController
@RequestMapping("/investment")
public class InvestmentController {
  @Autowired
  InvestmentService investmentService;

  @GetMapping(path = "/get", produces = {"application/json"})
  @ResponseBody
  public ResponseEntity<Iterable<InvestmentModel>> get() {
    return ResponseEntity.ok(investmentService.getAll());
  }

  @GetMapping(path = "/getbyuserid/{userid}", produces = {"application/json"})
  @ResponseBody
  public ResponseEntity<Iterable<InvestmentModel>> getByUserId(
      @PathVariable(value = "userid") String userId) {
    return ResponseEntity.ok(investmentService.getByUserId(userId));
  }

  @PostMapping(path = "", produces = {"application/json"})
  @ResponseBody
  public ResponseEntity process(@RequestBody Map<String, String> json) {
    return ResponseEntity.ok(investmentService
        .handle(json.get("senderId"), json.get("username"), json.get("message")));
  }
}
