package com.bot.mymoney.model;

/**
 * Model for user.
 **/
public class User {
  private String userId;
  private String name;
  private String remind;

  /**
   * User.
   **/
  public User(String userId, String name, String remind) {
    this.userId = userId;
    this.name = name;
    this.remind = remind;
  }

  /**
   * Getter.
   **/
  public String getUserId() {
    return userId;
  }

  /**
   * Setter.
   **/
  public void setUserId(String userId) {
    this.userId = userId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getReminder() {
    return remind;
  }

  public void setReminder(String remind) {
    this.remind = remind;
  }
}