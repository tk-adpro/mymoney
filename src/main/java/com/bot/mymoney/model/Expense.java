package com.bot.mymoney.model;

/**
 * Model untuk pengeluaran.
 **/
public class Expense {
  private String userId;
  private String name;
  private String category;
  private String timestamp;
  private String nominal;

  /**
   * Expense model constructor.
   **/
  public Expense(String userId, String name, String category, String timestamp, String nominal) {
    this.userId = userId;
    this.name = name;
    this.category = category;
    this.timestamp = timestamp;
    this.nominal = nominal;
  }

  public String getUserId() {
    return this.userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public String getName() {
    return this.name;
  }

  public void setName(String title) {
    this.name = title;
  }

  public String getCategory() {
    return this.category;
  }

  public void setCategory(String category) {
    this.category = category;
  }

  public String getTimestamp() {
    return this.timestamp;
  }

  public void setTimestamp(String timestamp) {
    this.timestamp = timestamp;
  }

  public String getNominal() {
    return this.nominal;
  }

  public void setNominal(String nominal) {
    this.nominal = nominal;
  }
}
