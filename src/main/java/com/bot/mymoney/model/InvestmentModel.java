package com.bot.mymoney.model;

/**
 * Model for investment manager.
 **/
public class InvestmentModel {
  String userId;
  String username;
  String category;
  String timeSpan;
  String mode;
  Integer funds;
  int time;
  int rate;
  Integer contrib;
  Integer result;

  /**
   * Investment model constructor.
   **/
  public InvestmentModel(String userId, String username, String category,
                         String timeSpan, String mode, Integer funds,
                         int time, int rate, Integer contrib, Integer result) {
    this.userId = userId;
    this.username = username;
    this.category = category;
    this.timeSpan = timeSpan;
    this.mode = mode;
    this.funds = funds;
    this.time = time;
    this.rate = rate;
    this.contrib = contrib;
    this.result = result;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public String getCategory() {
    return category;
  }

  public void setCategory(String category) {
    this.category = category;
  }

  public String getTimeSpan() {
    return timeSpan;
  }

  public void setTimeSpan(String timeSpan) {
    this.timeSpan = timeSpan;
  }

  public String getMode() {
    return mode;
  }

  public void setMode(String mode) {
    this.mode = mode;
  }

  public Integer getFunds() {
    return funds;
  }

  public void setFunds(Integer funds) {
    this.funds = funds;
  }

  public int getTime() {
    return time;
  }

  public void setTime(int time) {
    this.time = time;
  }

  public int getRate() {
    return rate;
  }

  public void setRate(int rate) {
    this.rate = rate;
  }

  public Integer getContrib() {
    return contrib;
  }

  public void setContrib(Integer contrib) {
    this.contrib = contrib;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public Integer getResult() {
    return result;
  }

  public void setResult(Integer result) {
    this.result = result;
  }
}
