package com.bot.mymoney.handler.record;

/**
 * Template code that every handler implement it with their needs.
 **/
public abstract class PendapatanTemplate {
  protected PendapatanHandler handler;
  protected String description;
  protected String messageToUser;

  public abstract PendapatanTemplate unknownMessage();

  /**
   * Process when user want to cancel or input is out of expectations .
   **/
  public PendapatanTemplate cancelOperation(String message) {
    if (message.equalsIgnoreCase("batal")) {
      messageToUser = "Proses fitur dibatalkan";
      return null;
    }
    return unknownMessage();
  }

  public String getMessageToUser() {
    return messageToUser;
  }
}
