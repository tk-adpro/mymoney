package com.bot.mymoney.handler.record;

/**
 * Process when user verify their nominal input.
 **/
public class PendapatanKonfirmasiHandler extends PendapatanTemplate implements PendapatanHandler {
  public PendapatanKonfirmasiHandler(PendapatanNominalHandler handler) {
    this.handler = handler;
  }

  @Override
  public PendapatanTemplate verificationMessage(String message) {
    if (message.equalsIgnoreCase("ya")) {
      return handle(message);
    }
    return cancelOperation(message);
  }

  @Override
  public PendapatanTemplate handle(String message) {
    messageToUser = "Pendapatan kamu berhasil dicatat!";
    description = "";
    return null;
  }

  @Override
  public String getDescription() {
    return handler.getDescription() + ";" + description;
  }

  @Override
  public PendapatanTemplate unknownMessage() {
    messageToUser = "Konfirmasi pencatatan dengan menjawab 'Ya' "
                        + "atau ketik 'Batal' untuk membatalkan tindakan";
    return this;
  }
}
