package com.bot.mymoney.handler.record;


/**
 * Handler that focus in handling message.
 **/
public interface PendapatanHandler {
  PendapatanTemplate verificationMessage(String message);

  PendapatanTemplate handle(String message);

  String getDescription();
}
