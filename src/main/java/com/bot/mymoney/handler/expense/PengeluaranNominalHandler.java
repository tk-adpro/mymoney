package com.bot.mymoney.handler.expense;

/**
 * Handler untuk input nominal catat pengeluaran.
 **/
public class PengeluaranNominalHandler extends PengeluaranHandler {

  public PengeluaranNominalHandler(PengeluaranKategoriHandler handler) {
    this.handler = handler;
    this.description = "";
  }

  @Override
  public PengeluaranHandler unknownMessage() {
    messageToUser = "Nominal uang tidak sesuai. "
                        + "Pastikan kamu hanya memasukkan angka, contoh: 50000. "
                        + "Jika ingin membatalkan tindakan, ketik 'Batal'";
    return this;
  }

  @Override
  public PengeluaranHandler verificationMessage(String message) {
    if (isNominal(message)) {
      return handle(message);
    } else {
      return cancelOperation(message);
    }
  }

  @Override
  public PengeluaranHandler handle(String message) {
    description = message;
    messageToUser = "Oke, uang yang digunakan sebesar Rp" + message
                        + ". Konfirmasi pencatatan dengan menjawab 'Ya' "
                        + "atau ketik 'Batal' untuk membatalkan tindakan";
    return new PengeluaranKonfirmasiHandler(this);
  }

  @Override
  public String getDescription() {
    return handler.getDescription() + ";" + this.description;
  }

  private boolean isNominal(String userMessage) {
    try {
      return Integer.parseInt(userMessage) > 0;
    } catch (NumberFormatException e) {
      return false;
    }
  }
}
