package com.bot.mymoney.handler.expense;

import java.util.Arrays;
import java.util.List;

/**
 * Handler untuk pemilihan kategori catat pengeluaran.
 **/
public class PengeluaranKategoriHandler extends PengeluaranHandler {
  private final List<String> kategoriPengeluaran =
      Arrays.asList("konsumsi", "transportasi", "utilitas", "belanja", "lainnya");

  public PengeluaranKategoriHandler(String userId, String name) {
    this.description = userId + ";" + name;
  }

  @Override
  public PengeluaranHandler unknownMessage() {
    messageToUser = "Kategori yang dipilih tidak ada"
                        + ", tolong pilih salah satu dari kategori yang tersedia."
                        + " Ketik 'Batal' jika ingin membatalkan.";
    return this;
  }

  @Override
  public PengeluaranHandler verificationMessage(String message) {
    if (kategoriPengeluaran.contains(message)) {
      return handle(message);
    } else {
      return cancelOperation(message);
    }
  }

  @Override
  public PengeluaranHandler handle(String message) {
    description += ";" + message;
    messageToUser = "Kategori " + message + " berhasil terpilih."
                        + " Berapa jumlah uang yang ingin kamu masukkan ke catatan pengeluaran?"
                        + " Contoh: 50000";
    return new PengeluaranNominalHandler(this);
  }

  @Override
  public String getDescription() {
    return this.description;
  }
}
