package com.bot.mymoney.handler.investment;

import com.bot.mymoney.handler.investment.calculator.CalculatorHandler;
import com.bot.mymoney.handler.investment.calculator.InvestmentCalculator;

/**
 * Contrib input handler.
 */
public class CalculatorHandlerContrib extends CalculatorHandler {
  public CalculatorHandlerContrib(String userId, String username, InvestmentCalculator calculator) {
    super(userId, username, calculator);
  }

  @Override
  public InvestmentHandler handle(String message) {
    Integer value = Integer.valueOf(message);
    calculator.input("contrib", value);
    description = "Investment Contribution added to calculator.";
    messageToUser = "Kontribusi per " + calculator.getSpan()
                        + " senilai " + message + " telah dipilih. \n"
                        + "Mohon konfirmasi apakah " + calculator.getData() + " benar. (Ya/Tidak)";
    return new CalculatorHandlerConfirm(userId, username, calculator);
  }
}
