package com.bot.mymoney.handler.investment;

/**
 * Handler for successful operation.
 */
public class StateHandlerSuccess extends InvestmentHandler {

  /**
   * Constructor untuk StateHandlerSuccess.
   */
  public StateHandlerSuccess(String userId, String username) {
    super(userId, username);
    description = "All operations successful.";
    messageToUser = "Operasi berhasil dijalankan.";
  }

  @Override
  public InvestmentHandler verificationMessage(String rawMessage) {
    return null;
  }

  @Override
  public InvestmentHandler handle(String message) {
    return null;
  }
}
