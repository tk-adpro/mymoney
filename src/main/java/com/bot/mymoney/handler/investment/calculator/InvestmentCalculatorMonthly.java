package com.bot.mymoney.handler.investment.calculator;

/**
 * Calculator for monthly investment.
 */
public class InvestmentCalculatorMonthly extends InvestmentCalculator {
  public InvestmentCalculatorMonthly() {
    super("bulan");
  }

  @Override
  public void calculateInvestment() {
    totalContrib = vars.get("time") * vars.get("contrib") * 12;
    int current = vars.get("funds");
    int rate = vars.get("rate");
    try {
      for (int i = 0; i < vars.get("time"); i++) {
        current += vars.get("contrib") * (12 - mode);
        current += current * rate / 100;
        current += vars.get("contrib") * mode;

      }
    } catch (Exception e) {
      System.out.println("Calculator error: " + e);
    }
    result = current;
  }
}
