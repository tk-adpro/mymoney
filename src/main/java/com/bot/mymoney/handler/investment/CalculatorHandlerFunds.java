package com.bot.mymoney.handler.investment;

import com.bot.mymoney.handler.investment.calculator.CalculatorHandler;
import com.bot.mymoney.handler.investment.calculator.InvestmentCalculator;

/**
 * Fund input handler.
 */
public class CalculatorHandlerFunds extends CalculatorHandler {
  public CalculatorHandlerFunds(String userId, String username, InvestmentCalculator calculator) {
    super(userId, username, calculator);
  }

  @Override
  public InvestmentHandler handle(String message) {
    Integer value = Integer.valueOf(message);
    calculator.input("funds", value);
    description = "Starting Investment added to calculator.";
    messageToUser = "Investasi awal senilai " + message + " telah dipilih. \n"
                        + "Mohon isi janka waktu investasi dalam tahun.";
    return new CalculatorHandlerTime(userId, username, calculator);
  }
}
