package com.bot.mymoney.handler.investment;

import com.bot.mymoney.handler.investment.calculator.CalculatorHandler;
import com.bot.mymoney.handler.investment.calculator.InvestmentCalculator;

/**
 * Time input handler.
 */
public class CalculatorHandlerTime extends CalculatorHandler {
  public CalculatorHandlerTime(String userId, String username, InvestmentCalculator calculator) {
    super(userId, username, calculator);
  }

  @Override
  public InvestmentHandler handle(String message) {
    Integer value = Integer.valueOf(message);
    calculator.input("time", value);
    description = "Investment Timespan added to calculator.";
    messageToUser = "Janka waktu " + message + " tahun telah dipilih. \n"
                        + "Mohon isi bunga investasi per "
                        + calculator.getSpan() + " dalam X persen.";
    return new CalculatorHandlerRate(userId, username, calculator);
  }
}
