package com.bot.mymoney.handler.investment;

import com.bot.mymoney.handler.investment.calculator.InvestmentCalculator;
import com.bot.mymoney.handler.investment.calculator.InvestmentCalculatorAnnually;
import com.bot.mymoney.handler.investment.calculator.InvestmentCalculatorMonthly;
import java.util.HashMap;
import java.util.Map;

/**
 * Category input handler.
 */
public class CalculatorHandlerCategory extends InvestmentHandler {
  Map<String, Integer> modes;
  Map<String, InvestmentCalculator> calculators;

  {
    modes = new HashMap<>();
    modes.put("awal", 0);
    modes.put("akhir", 1);
  }

  {
    calculators = new HashMap<>();
    calculators.put("bulan", new InvestmentCalculatorMonthly());
    calculators.put("tahun", new InvestmentCalculatorAnnually());
  }

  public CalculatorHandlerCategory(String userId, String username) {
    super(userId, username);
  }

  @Override
  public InvestmentHandler verificationMessage(String rawMessage) {
    if (rawMessage.equalsIgnoreCase("batal")) {
      return new StateHandlerCancel(userId, username);
    }

    StateHandlerFail failState = new StateHandlerFail(userId, username);
    try {
      if (calculators.containsKey(rawMessage.split(" ")[1].toLowerCase())) {
        return handle(rawMessage.toLowerCase());
      }
      failState.verificationMessage("Unknown Message.");
    } catch (Exception e) {
      failState.verificationMessage("Error occurred.");
    }

    return failState;
  }

  @Override
  public InvestmentHandler handle(String message) {
    InvestmentCalculator calculator = calculators.get(message.split(" ")[1]);
    calculator.setMode(modes.get(message.split(" ")[0]));
    messageToUser = "Kalkulator mode " + message + " telah dipilih. \n"
                        + "Mohon isi nilai investasi awal.";
    description = "Calculator selected.";
    return new CalculatorHandlerFunds(userId, username, calculator);
  }
}
