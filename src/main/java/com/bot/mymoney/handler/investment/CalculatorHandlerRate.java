package com.bot.mymoney.handler.investment;

import com.bot.mymoney.handler.investment.calculator.CalculatorHandler;
import com.bot.mymoney.handler.investment.calculator.InvestmentCalculator;

/**
 * Rate input handler.
 */
public class CalculatorHandlerRate extends CalculatorHandler {
  public CalculatorHandlerRate(String userId, String username, InvestmentCalculator calculator) {
    super(userId, username, calculator);
  }

  @Override
  public InvestmentHandler handle(String message) {
    Integer value = Integer.valueOf(message);
    calculator.input("rate", value);
    description = "Investment Interest rate added to calculator.";
    messageToUser = "Bunga investasi per " + calculator.getSpan()
                        + " senilai " + message + "% telah dipilih. \n"
                        + "Mohon isi kontribusi per " + calculator.getSpan() + ".";
    return new CalculatorHandlerContrib(userId, username, calculator);
  }
}
