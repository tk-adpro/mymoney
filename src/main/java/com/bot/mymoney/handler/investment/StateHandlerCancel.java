package com.bot.mymoney.handler.investment;

/**
 * Handler for cancel operation.
 */
public class StateHandlerCancel extends InvestmentHandler {
  /**
   * Constructor for StateHandlerCancel.
   */
  public StateHandlerCancel(String userId, String username) {
    super(userId, username);
    description = "Operation Cancelled.";
    messageToUser = "Operasi berhasil dibatalkan.";
  }

  @Override
  public InvestmentHandler verificationMessage(String rawMessage) {
    return null;
  }

  @Override
  public InvestmentHandler handle(String message) {
    return null;
  }
}
