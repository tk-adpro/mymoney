package com.bot.mymoney.handler.investment.calculator;

import com.bot.mymoney.handler.investment.InvestmentHandler;
import com.bot.mymoney.handler.investment.StateHandlerCancel;
import com.bot.mymoney.handler.investment.StateHandlerFail;
import java.util.Objects;

/**
 * Handler for calculator.
 */
public abstract class CalculatorHandler extends InvestmentHandler {
  public final InvestmentCalculator calculator;

  public CalculatorHandler(String userId, String username, InvestmentCalculator calculator) {
    super(userId, username);
    this.calculator = calculator;
  }

  @Override
  public InvestmentHandler verificationMessage(String rawMessage) {
    if (rawMessage.equalsIgnoreCase("batal")) {
      return new StateHandlerCancel(userId, username);
    }
    StateHandlerFail failState;
    try {
      InvestmentHandler nextHandler = handle(rawMessage);
      Objects.requireNonNull(nextHandler);
      return nextHandler;
    } catch (NumberFormatException e) {
      failState = new StateHandlerFail(userId, username);
      failState.verificationMessage("Could not Parse.");
    } catch (NullPointerException e) {
      failState = new StateHandlerFail(userId, username);
      failState.verificationMessage("Unknown message received.");
    }
    return failState;
  }
}
