package com.bot.mymoney.handler.investment.calculator;

import java.util.HashMap;
import java.util.Map;

/**
 * Calculator abstract.
 */
public abstract class InvestmentCalculator {
  public final Map<String, Integer> vars = new HashMap<>();
  protected final String span;
  protected Integer mode;
  protected Integer result;
  protected Integer totalContrib;

  /**
   * Constructor.
   */
  public InvestmentCalculator(String span) {
    this.span = span;
    mode = 0;
    vars.put("funds", 100000);
    vars.put("time", 10);
    vars.put("rate", 8);
    vars.put("contrib", 30000);
  }

  public void setMode(Integer mode) {
    this.mode = mode;
  }

  public String getSpan() {
    return span;
  }

  public void input(String variable, Integer value) {
    vars.put(variable, value);
  }

  public abstract void calculateInvestment() throws ArithmeticException;

  /**
   * Get data.
   */
  public String getData() {
    return "|Investasi Awal: " + vars.get("funds") + ","
               + "Waktu Investasi: " + vars.get("time") + ","
               + "Bunga Investasi: " + vars.get("rate") + ","
               + "Contribusi per " + span + ": " + vars.get("contrib") + "|";
  }

  /**
   * Get result.
   */
  public String getResults() {
    return "Anda akan memperoleh keuntungan sebesar "
               + (result - totalContrib - vars.get("funds"))
               + " dengan nilai total sebesar " + result + "." + "\n"
               + "Dari investasi awal sebesar " + vars.get("funds") + "\n"
               + "dengan kontribusi total " + totalContrib + "." + "\n"
               + "Yang diinvestasikan selama " + vars.get("time") + "\n"
               + "dengan bunga investasi " + vars.get("rate") + ".";
  }

  public Integer getResult() {
    return result;
  }
}
