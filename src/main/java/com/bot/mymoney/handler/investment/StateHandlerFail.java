package com.bot.mymoney.handler.investment;

/**
 * Handler for failed operation.
 */
public class StateHandlerFail extends InvestmentHandler {
  /**
   * Constructor for StateHandlerFail.
   */
  public StateHandlerFail(String userId, String username) {
    super(userId, username);
    messageToUser = "Operasi gagal dijalankan.";
  }

  @Override
  public InvestmentHandler verificationMessage(String description) {
    this.description = description;
    return null;
  }

  @Override
  public InvestmentHandler handle(String message) {
    return null;
  }
}
