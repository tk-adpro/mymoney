package com.bot.mymoney.handler.investment;

import com.bot.mymoney.handler.investment.calculator.CalculatorHandler;
import com.bot.mymoney.handler.investment.calculator.InvestmentCalculator;

/**
 * Confirm input handler.
 */
public class CalculatorHandlerConfirm extends CalculatorHandler {
  public CalculatorHandlerConfirm(String userId, String username, InvestmentCalculator calculator) {
    super(userId, username, calculator);
  }

  @Override
  public InvestmentHandler handle(String message) {
    try {
      if (!message.equalsIgnoreCase("ya")) {
        throw new NullPointerException("Unknown message.");
      }
      calculator.calculateInvestment();
      description = "Investment Calculated, result " + calculator.getResult() + ".";
      messageToUser = calculator.getResults();
      return new StateHandlerSuccess(userId, username);
    } catch (Exception e) {
      return new StateHandlerFail(userId, username).verificationMessage("Operation Failed: " + e);
    }
  }
}
