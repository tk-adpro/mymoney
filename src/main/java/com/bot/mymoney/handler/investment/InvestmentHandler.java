package com.bot.mymoney.handler.investment;

/**
 * Handler abstract class.
 */
public abstract class InvestmentHandler {
  protected String userId;
  protected String username;
  protected String messageToUser;
  protected String description;

  public InvestmentHandler(String userId, String username) {
    this.userId = userId;
    this.username = username;
  }

  public abstract InvestmentHandler verificationMessage(String rawMessage);

  public abstract InvestmentHandler handle(String message);

  public String getMessageToUser() {
    return messageToUser;
  }

  public String getDescription() {
    return description;
  }
}
