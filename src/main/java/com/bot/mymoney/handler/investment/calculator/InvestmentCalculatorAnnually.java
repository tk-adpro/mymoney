package com.bot.mymoney.handler.investment.calculator;

/**
 * Calculator for annually investment.
 */
public class InvestmentCalculatorAnnually extends InvestmentCalculator {
  public InvestmentCalculatorAnnually() {
    super("tahun");
  }

  @Override
  public void calculateInvestment() {
    totalContrib = vars.get("time") * vars.get("contrib");
    int current = vars.get("funds");
    int rate = vars.get("rate");
    current += mode == 0 ? 0 : current * rate / 100;
    try {
      for (int i = 0; i < vars.get("time") - mode; i++) {
        System.out.println(current);
        current += vars.get("contrib");
        System.out.println(current);
        current += current * rate / 100;
      }
    } catch (Exception e) {
      System.out.println("Calculator error: " + e);
    }
    result = mode == 0 ? current : current + vars.get("contrib");
  }
}
