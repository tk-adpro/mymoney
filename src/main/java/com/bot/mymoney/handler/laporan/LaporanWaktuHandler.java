package com.bot.mymoney.handler.laporan;

import com.bot.mymoney.model.Expense;
import com.bot.mymoney.model.Record;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import org.springframework.web.client.RestTemplate;

/**
 * LaporanWaktuHandler untuk memilih waktu dan mengeluarkan output nominal berdasarkan waktu.
 **/
public class LaporanWaktuHandler extends LaporanHandler {
  public static final List<String> kategoriWaktu = Arrays.asList("harian", "mingguan", "bulanan");
  final String url = "https://mymoneyy.herokuapp.com/";
  LaporanCatatanHandler catatan;
  RestTemplate restTemplate;

  /**
   * LaporanWaktuHandler constructor sebagai catatan untuk catatanhandler.
   **/
  public LaporanWaktuHandler(LaporanCatatanHandler catatan) {
    this.catatan = catatan;
    this.description = "";
    restTemplate = new RestTemplate();
  }

  @Override
  public LaporanHandler unknownMessage() {
    messageToUser = "Kategori yang dipilih tidak ada, tolong pilih salah satu dari "
                        + "kategori yang tersedia. Ketik 'Batal' jika ingin membatalkan.";
    return this;
  }

  @Override
  public LaporanHandler verificationMessage(String message) {
    if (kategoriWaktu.contains(message)) {
      return handle(message);
    } else {
      return cancelOperation(message);
    }
  }

  @Override
  public LaporanHandler handle(String message) {
    description = catatan.getDescription() + ";" + message;
    int days = this.stringToDay(message);
    String catat = getDescription().split(";")[2];
    messageToUser = categoryCatatan(catat, days);
    return null;
  }

  @Override
  public String getDescription() {
    return catatan.getDescription();
  }

  private String categoryCatatan(String catat, int days) {
    if (catat.equalsIgnoreCase("laporan pengeluaran")) {
      return stringNominalExpense(days);
    }
    return stringNominalRecord(days);
  }

  private String stringNominalExpense(int hari) {
    String userId = getDescription().split(";")[0];
    LocalDateTime dateNow = LocalDateTime.now().plusHours(7);

    List<Expense> expenses = extractExpense(userId, dateNow, hari);
    HashMap<String, Integer> nominalExpense = calculateNominalExpense(expenses);
    String messageResult = "Hasil perhitungan dari Pengeluaran untuk "
                               + hari + " hari terakhir adalah: \n"
                               + "konsumsi = Rp." + nominalExpense.get("konsumsi") + ",- \n"
                               + "transportasi = Rp." + nominalExpense.get("transportasi") + ",- \n"
                               + "utilitas = Rp." + nominalExpense.get("utilitas") + ",- \n"
                               + "belanja = Rp." + nominalExpense.get("utilitas") + ",- \n"
                               + "total = Rp." + nominalExpense.get("total") + ",-";

    return messageResult;
  }

  private String stringNominalRecord(int hari) {
    String userId = getDescription().split(";")[0];
    LocalDateTime dateNow = LocalDateTime.now().plusHours(7);

    List<Record> records = extractRecord(userId, dateNow, hari);
    HashMap<String, Integer> nominalRecord = calculateNominalRecord(records);
    return "Hasil perhitungan dari Pendapatan untuk " + hari
               + " hari terakhir adalah: \n"
               + "Gaji = Rp." + nominalRecord.get("gaji") + ",- \n"
               + "Usaha = Rp." + nominalRecord.get("usaha") + ",- \n"
               + "lainnya = Rp." + nominalRecord.get("lainnya") + ",- \n"
               + "total = Rp." + nominalRecord.get("total") + ",-";
  }

  private HashMap<String, Integer> calculateNominalExpense(List<Expense> expenses) {
    //Initialize category to HashMap
    HashMap<String, Integer> categoryNominal = new HashMap<>();
    categoryNominal.put("konsumsi", 0);
    categoryNominal.put("transportasi", 0);
    categoryNominal.put("utilitas", 0);
    categoryNominal.put("belanja", 0);
    categoryNominal.put("lainnya", 0);

    //count category nominal
    int totalNominal = 0;
    for (Expense expense : expenses) {
      int nominalExpense = Integer.parseInt(expense.getNominal());
      String categoryExpense = expense.getCategory();
      categoryNominal.put(categoryExpense,
          categoryNominal.get(categoryExpense) + nominalExpense);
      totalNominal += nominalExpense;
    }
    categoryNominal.put("total", totalNominal);
    return categoryNominal;
  }

  private HashMap<String, Integer> calculateNominalRecord(List<Record> records) {
    //Initialize category to HashMap
    HashMap<String, Integer> categoryNominal = new HashMap<>();
    categoryNominal.put("gaji", 0);
    categoryNominal.put("usaha", 0);
    categoryNominal.put("lainnya", 0);

    //count category nominal
    int totalNominal = 0;
    for (Record record : records) {
      int nominalRecord = Integer.parseInt(record.getNominal());
      String categoryRecord = record.getCategory();
      categoryNominal.put(categoryRecord,
          categoryNominal.get(categoryRecord) + nominalRecord);
      totalNominal += nominalRecord;
    }
    categoryNominal.put("total", totalNominal);
    return categoryNominal;
  }

  private List<Expense> extractExpense(String userId, LocalDateTime time, int hari) {
    List<Expense> expenses = new ArrayList<>();
    String response = restTemplate.getForObject(url
        + "expense/getbyuserid/" + userId, String.class);
    response = Objects.requireNonNull(response).substring(1, response.length() - 1)
                   .replace("}", "")
                   .replace("{", "")
                   .replace("\"", "")
                   .replace("userId:", "")
                   .replace("name:", "")
                   .replace("category:", "")
                   .replace("timestamp:", "")
                   .replace("nominal:", "");

    String[] dataAsArray = response.split(",");

    for (int i = 0; i < dataAsArray.length / 5; i++) {
      LocalDateTime timestamp = LocalDateTime.parse(dataAsArray[i * 5 + 3]);
      if (Duration.between(timestamp, time).toDays() < hari) {
        expenses.add(new Expense(
            dataAsArray[i * 5 + 0],
            dataAsArray[i * 5 + 1],
            dataAsArray[i * 5 + 2],
            dataAsArray[i * 5 + 3],
            dataAsArray[i * 5 + 4]
        ));
      }
    }

    return expenses;
  }

  private List<Record> extractRecord(String userId, LocalDateTime time, int hari) {
    List<Record> records = new ArrayList<>();
    String response = restTemplate.getForObject(url
                                                    + "record/getbyuserid/" + userId, String.class);
    response = Objects.requireNonNull(response).substring(1, response.length() - 1)
                   .replace("}", "")
                   .replace("{", "")
                   .replace("\"", "")
                   .replace("userId:", "")
                   .replace("name:", "")
                   .replace("category:", "")
                   .replace("timestamp:", "")
                   .replace("nominal:", "");

    String[] dataAsArray = response.split(",");

    for (int i = 0; i < dataAsArray.length / 5; i++) {
      LocalDateTime timestamp = LocalDateTime.parse(dataAsArray[i * 5 + 3]);
      if (Duration.between(timestamp, time).toDays() < hari) {
        records.add(new Record(
            dataAsArray[i * 5 + 0],
            dataAsArray[i * 5 + 1],
            dataAsArray[i * 5 + 2],
            dataAsArray[i * 5 + 3],
            dataAsArray[i * 5 + 4]
        ));
      }
    }

    return records;
  }

  private int stringToDay(String message) {
    if (message.equalsIgnoreCase("Harian")) {
      return 1;
    } else if (message.equalsIgnoreCase("Mingguan")) {
      return 7;
    } else {
      return 30;
    }
  }

}
