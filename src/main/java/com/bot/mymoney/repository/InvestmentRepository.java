package com.bot.mymoney.repository;

import com.bot.mymoney.database.InvestmentDao;
import com.bot.mymoney.model.InvestmentModel;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * InvestmentDb.
 **/
@Repository
public class InvestmentRepository {

  @Autowired
  private InvestmentDao investmentDao;

  public List<InvestmentModel> getAll() {
    return investmentDao.get();
  }

  public List<InvestmentModel> getByUserId(String userId) {
    return investmentDao.getByUserId(userId);
  }

  /**
   * Save investment to database.
   */
  public int saveInvestment(String desc) {
    String[] userData = desc.split(";");
    return investmentDao.saveInvestment(userData[0], userData[1],
        userData[2], userData[3], userData[4], Integer.valueOf(userData[5]),
        Integer.parseInt(userData[6]), Integer.parseInt(userData[7]),
        Integer.valueOf(userData[8]), Integer.valueOf(userData[9]));
  }
}
