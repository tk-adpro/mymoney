package com.bot.mymoney.repository;

import com.bot.mymoney.database.UserDao;
import com.bot.mymoney.model.User;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * UserDb to control tbl_user in database.
 **/
@Repository
public class UserRepository {

  @Autowired
  private UserDao userDao;

  /**
   * Input new user to database.
   **/
  public int registerUser(String userId, String name) {
    if (getUserById(userId) == null) {
      return userDao.registerUser(userId, name);
    }
    return -1;
  }

  /**
   * Get user by id.
   **/
  public String getUserById(String userId) {
    List<User> users = userDao.getByUserId("%" + userId + "%");
    if (users.size() > 0) {
      return users.get(0).getUserId();
    }
    return null;
  }

  public List<User> getAllUsers() {
    return userDao.get();
  }

  public List<User> getStatusNotifikasiByUserId(String userId) {
    return userDao.getStatusNotifikasiByUserId(userId);
  }

  /**
   * get all user that active notification.
   **/
  public Set<String> getAllUserNotifikasiAktif() {
    List<User> users = userDao.getAllUserNotifikasiAktif();
    List<String> userIdList = new ArrayList<>();
    for (User user : users) {
      userIdList.add(user.getUserId());
    }
    return new HashSet(userIdList);
  }

  public int setStatusNotifikasi(String status, String userId) {
    return userDao.setStatusNotifikasiByUserId(status, userId);
  }
}