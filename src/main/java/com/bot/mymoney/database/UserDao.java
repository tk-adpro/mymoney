package com.bot.mymoney.database;

import com.bot.mymoney.model.User;
import java.util.List;

/**
 * UserDao.
 **/
public interface UserDao {
  List<User> get();

  List<User> getByUserId(String userId);

  List<User> getStatusNotifikasiByUserId(String userId);

  int registerUser(String userId, String displayName);

  int setStatusNotifikasiByUserId(String status, String userId);

  List<User> getAllUserNotifikasiAktif();
}
