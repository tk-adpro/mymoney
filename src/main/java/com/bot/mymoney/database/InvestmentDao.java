package com.bot.mymoney.database;

import com.bot.mymoney.model.InvestmentModel;
import java.util.List;

/**
 * InvestmentDao.
 **/
public interface InvestmentDao {
  List<InvestmentModel> get();

  List<InvestmentModel> getByUserId(String userId);

  int saveInvestment(String userId, String name,
                     String category, String timeSpan,
                     String mode, Integer funds,
                     int time, int rate,
                     Integer contrib, Integer result);
}
