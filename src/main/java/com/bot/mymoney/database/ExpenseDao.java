package com.bot.mymoney.database;

import com.bot.mymoney.model.Expense;
import java.util.List;

/**
 * Akses database untuk pengeluaran.
 **/
public interface ExpenseDao {
  List<Expense> get();

  List<Expense> getByUserId(String userId);

  int saveExpense(String userId, String name, String category, String timestamp, String nominal);
}
