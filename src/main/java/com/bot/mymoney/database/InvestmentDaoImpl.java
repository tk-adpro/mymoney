package com.bot.mymoney.database;

import com.bot.mymoney.model.InvestmentModel;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

/**
 * InvestmentDaoImp.
 **/
public class InvestmentDaoImpl implements InvestmentDao {
  private static final String INVESTMENT_TABLE = "tbl_investment";
  private static final String SQL_SELECT_ALL = "SELECT * FROM " + INVESTMENT_TABLE;
  private static final ResultSetExtractor<List<InvestmentModel>>
      INVESTMENT_EXTRACTOR = InvestmentDaoImpl::extractData;
  private JdbcTemplate jdbcTemplate;

  public InvestmentDaoImpl(DataSource dataSource) {
    jdbcTemplate = new JdbcTemplate(dataSource);
  }

  /**
   * extract data for InvestmentModel table.
   **/
  public static List<InvestmentModel> extractData(ResultSet resultSet) throws SQLException {
    List<InvestmentModel> extractList = new ArrayList<>();
    while (resultSet.next()) {
      InvestmentModel sp = new InvestmentModel(
          resultSet.getString("user_id"),
          resultSet.getString("name"),
          resultSet.getString("category"),
          resultSet.getString("timespan"),
          resultSet.getString("mode"),
          resultSet.getInt("funds"),
          resultSet.getInt("time"),
          resultSet.getInt("rate"),
          resultSet.getInt("contrib"),
          resultSet.getInt("result"));
      extractList.add(sp);
    }
    return extractList;
  }

  @Override
  public List<InvestmentModel> get() {
    return jdbcTemplate.query(SQL_SELECT_ALL, INVESTMENT_EXTRACTOR);
  }

  @Override
  public List<InvestmentModel> getByUserId(String userId) {
    String sqlGetByUserId = SQL_SELECT_ALL + " WHERE LOWER(user_id) LIKE LOWER(?);";
    return jdbcTemplate.query(sqlGetByUserId,
        new Object[]{"%" + userId + "%"},
        INVESTMENT_EXTRACTOR);
  }

  @Override
  public int saveInvestment(String userId, String name,
                            String category, String timeSpan,
                            String mode, Integer funds,
                            int time, int rate,
                            Integer contrib, Integer result) {
    String register = "INSERT INTO " + INVESTMENT_TABLE
                          + " (user_id, name, category, timeSpan,"
                          + " mode, funds, time, rate, contrib, result)"
                          + " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
    return jdbcTemplate.update(register, userId,
        name, category, timeSpan, mode,
        funds, time, rate, contrib, result);
  }
}
