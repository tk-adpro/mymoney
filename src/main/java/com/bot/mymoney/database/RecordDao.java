package com.bot.mymoney.database;

import com.bot.mymoney.model.Record;
import java.util.List;

/**
 * for RecordDao Implementation.
 **/
public interface RecordDao {
  List<Record> get();

  List<Record> getByUserId(String userId);

  int saveRecord(String userId, String name, String category,
                 String timestamp, String nominal);
}